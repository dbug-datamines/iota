<?php /*%%SmartyHeaderCode:7560054985b1eb09df35f51-64268870%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c8f0e8d42b2158676cdd5fdf361b3dd8ee54b061' => 
    array (
      0 => '/var/www/html/application/templates/recommend.tpl',
      1 => 1528885283,
      2 => 'file',
    ),
    'edd1cb73fee0db744b1e53ae1195f20ef697e75c' => 
    array (
      0 => '/var/www/html/application/templates/header_recomm.tpl',
      1 => 1528884849,
      2 => 'file',
    ),
    '14fe32dc7fc0f1796c14af6b047011c855c96e12' => 
    array (
      0 => '/var/www/html/application/templates/footer_recomm.tpl',
      1 => 1528806002,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7560054985b1eb09df35f51-64268870',
  'cache_lifetime' => 3600,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5b20f0251e4cd3_42191666',
  'has_nocache_code' => true,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b20f0251e4cd3_42191666')) {function content_5b20f0251e4cd3_42191666($_smarty_tpl) {?> <!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Ask IOTA</title>
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/fonts.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/crumina-fonts.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/normalize.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/grid.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/styles.css">
<!--Plugins styles-->
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/primary-menu.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/magnific-popup.css">
<link href="https://www.askiota.com/assets/css/hexa.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://www.askiota.com/assets/css/bootstrap.min.css">
<link href="https://www.askiota.com/assets/css/smart_wizard.css" rel="stylesheet" type="text/css" />

<!-- Optional SmartWizard theme -->
<link href="https://www.askiota.com/assets/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />

<!--Styles for RTL-->
<!--<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets//css/rtl.css">-->
<!--External fonts-->
<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/iota-style.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/style.css">
<link rel='stylesheet' href='https://www.askiota.com/assets/css/rangeslider.css'>
<link rel='stylesheet' href='https://www.askiota.com/assets/css/rangeslider_override.css'>
<link rel="stylesheet" type="text/css" href='https://www.askiota.com/assets/css/starratting.css'>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120477304-1"></script>

    <script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120477304-1');
</script>
 

</head>
<body class=" ">
 
<!-- Header -->
<header class="header" >
  <div class="container">
    <div class="header-content-wrapper">
      <div class="logo"> <a href="<?php echo @constant('BASE_URL');?>
" class="full-block-link"></a> <img src="<?php echo @constant('ASSET_PATH');?>
/images/logo_new.png">
        <div class="logo-text"> 
          <!--  <div class="logo-title"><?php echo @constant('SITE_TITLE');?>
</div>
                                <div class="logo-sub-title"><?php echo @constant('SUB_SITE_TITLE');?>
</div>--> 
        </div>
      </div>
      <div class="pull-right translation" id="google_translate_element"></div>
    </div>
  </div>
</header>
<div class="modal rating-mobel-box fade" id="myModal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> <?php echo form_open('auth/add_ratting',$_smarty_tpl->tpl_vars['agent_login_form']->value);?>

      <div class="modal-header">
        <h4 class="modal-title">Rate Your Experience</h4>
      </div>
      <div class="modal-body"> <span class="my-rating-9"></span>
        <input type='hidden' name='rating' value="3.5" id='live-rating'>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-lg raised">SUBMIT</button>
        <?php echo form_close();?>

        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">LATER</button>
      </div>
    </div>
  </div>
</div>
<div class="modal rating-mobel-box fade" id="myModal_geo" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> 
      <div class="modal-body"> <span>You are Not having permission to login</span> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<div class="modal rating-mobel-box fade" id="myModal_error" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> 
      <div class="modal-body"> <span>Please update setting part</span> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<div class="modal myModal_iota_popup fade" id="myModal_iota_popup" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> 
      <div class="modal-header">
        <h4 class="modal-title">Rate Your Experience</h4>
      </div>
     
      <div class="modal-body"> <img src="assets/images/iota_imgl.png"> <div id="" class="left"> <h6>Hallo, Wij zijn Occhio!</h6></div> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
 
 
<!-- <style>
.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 25px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}
.carousel-inner.onebyone-carosel { margin: auto; width: 80%; }
.onebyone-carosel .active.left { left: -25.00%; }
.onebyone-carosel .active.right { left: 25.00%; }
.onebyone-carosel .next { left: 25.00%; }
.onebyone-carosel .prev { left: -25.00%; }
.left {
  display: none;
}
.right {
  display: none;
}
</style> -->
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row bg-border-color medium-padding70 pd-bt0">
      <div class="container">
        <div class="row">
          <nocache>
            <form action="#" id="recommForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
              <!-- SmartWizard html -->
              <div id="smartwizard">
                <ul>
                  <li><a href="#step-1"><span>Preferences</span> <small></small></a></li>
                  <li><a href="#step-2"><span>Taste</span> <small></small></a></li>
                  <li><a href="#step-3"><span>Base</span> <small></small></a></li>
                  <li><a href="#step-4"><span>Psychology</span> <small> </small></a></li>
                  <li><a href="#step-5"><span>Recommendations</span> <small> </small></a></li>
                </ul>
                <div>
                  <div id="step-1" class="ethnicity">
                    <div id="form-step-0" role="form" data-toggle="validator">
                      <div class="col-md-12 col-sm-12 ">
                        <div class="col-md-6 col-sm-6 col1">
                          <h4>ETHNICITY <span data-toggle="tooltip" data-placement="bottom" title="Ethnicity, noun,
the fact or state of belonging to a social group that has a common national or cultural tradition.
the interrelationship between gender, ethnicity, and class"> <i class="fa fa-question-circle"></i> </span></h4>
                          <div class="form-group position-relative">
                            <ul class="list list--secondary ethnicity_selection">
                              <div class="col-md-12 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">Maharashtrian </a>
                                  <input type="radio" name="chk1" id="item4" value="MAHARASHTRIAN" class="hidden" autocomplete="off" required>
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#"> Gujarati </a>
                                  <input type="radio" name="chk1" id="item4" value="GUJARATI" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#"> South Indian </a>
                                  <input type="radio" name="chk1" id="item4" value="SOUTH INDIAN" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#"> Punjabi </a>
                                  <input type="radio" name="chk1" id="item4" value="PUNJABI" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">Coastal </a>
                                  <input type="radio" name="chk1" id="item4" value="COASTAL" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-12 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">Others </a>
                                  <input type="radio" name="chk1" id="item4" value="OTHERS" class="hidden" autocomplete="off">
                                </li>
                              </div>
                            </ul>
                            <div class="help-block with-errors re-mg-bt"></div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col2">
                          <h4>PREFERENCE </h4>
                          <div class="col-md-12 col-sm-12 border pd0 pd-top10 pd-bottom8 mg-bt10 preference">
                            <div class="form-group radio-new">
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Veg"  autocomplete="off" required>
                                  Veg <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Non-Veg"  autocomplete="off">
                                  Non-Veg <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Jain"  autocomplete="off">
                                  Jain <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5 mg-top10">
                                <label class="radio-check tooltip-label">
                                  <input type="radio" name="chk2" id="item4" value="Vegan"  autocomplete="off">
                                  Vegan <span class="checkmark"></span> </label><span class="vegan" data-toggle="tooltip" data-placement="bottom" title="a person who does not eat or use animal products."> <i class="fa fa-question-circle"></i> </span>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5 mg-top10">
                                <label class="radio-check tooltip-label">
                                  <input type="radio" name="chk2" id="item4" value="Surprise-me"  autocomplete="off">
                                  Surprise me! <span class="checkmark"></span> </label><span class="vegan" data-toggle="tooltip" data-placement="bottom" title="Let IOTA select the one for you basis your other preferences"> <i class="fa fa-question-circle"></i> </span>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5 mg-top10">
                                <label class="radio-check tooltip-label">
                                  <input type="radio" name="chk2" id="item4" value="I'm on DIET"  autocomplete="off">
                                  I'm on DIET <span class="checkmark"></span> </label><span class="vegan" data-toggle="tooltip" data-placement="bottom" title="Low fat, healthy food to meet your health goals"> <i class="fa fa-question-circle"></i> </span>
                              </div>
                              <div class="help-block with-errors "></div>
                            </div>
                          </div>
                          <h4>GENDER </h4>
                          <div class="col-md-12 col-sm-12 border pd0 pd-top10 pd-bottom8 mg-bt10 gender">
                            <div class="form-group radio-new">
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk3" id="item4" value="Male"  autocomplete="off" required>
                                  Male <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk3" id="item4" value="Female" autocomplete="off">
                                  Female<span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk3" id="item4" value="Others" autocomplete="off">
                                  Others<span class="checkmark"></span> </label>
                              </div>
                              <div class="help-block with-errors"></div>
                            </div>
                          </div>
                          <h4> AGE</h4>
                          <div class="col-md-12 col-sm-12 border pd0 pd-top10 pd-bottom8 mg-bt10 clear">
                            <div class="wrapper-rangeslider ">
                              <input type="range" min="10" max="80" step="1" name="age" value="0"/>
                              <div id="ruler"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="step-2" role="form" data-toggle="validator" class="test">
                    <div id="form-step-1" role="form" data-toggle="validator">
                      <div class="col-md-12 col-sm-12 ">
                        <h4 class="h4pd mg-bt0">TASTE</h4>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block  pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Sweet</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_sweet" id="valR" class="slider" type="range" min="1" max="10" value="0" step="1"/>
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Spicy</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_spicy" id="valR1" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test1" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test1"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Cheesy</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_cheesy" id="valR2" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test2" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test2"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Gravy/Soupy</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_gravy_soupy" id="valR3" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test3" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test3"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Sour</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_sour" id="valR4" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test4" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test4"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="step-3">
                    <div id="form-step-2" role="form" data-toggle="validator">
                      <div class="col-md-12">
                        <div class="col-md-6 border-right">
                          <div class="form-group position-relative">
                            <h4 class="h4pd text-center">BASE </h4>
                            <ul class="list list--secondary  text-center">
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon1 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Chips/Crispy" class="hidden" autocomplete="off" required>
                                </label>
                                <p class="balloon-title"> Chips/Crispy </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon2 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Noodles" class="hidden" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Noodles 
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon3 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="No Base" class="hidden" autocomplete="off">
                                </label>
                                <p class="balloon-title"> No Base</p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon4 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Rice" class="hidden" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Rice </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon5 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Bread" class="hidden" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Bread </p>
                              </li>
                            </ul>
                            <div class="help-block with-errors step-3-error"></div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group position-relative ">
                            <h4 class="h4pd text-center"> INGREDIENTS</h4>
                            <ul class="list list--secondary  text-center">
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon6 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Garlic']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off" required>
                                </label>
                                <p class="balloon-title bg-none"> Garlic </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon7 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Coconut']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Coconut </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon8 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Tomato']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Tomato </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon9 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Mustard']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Mustard </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon10 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Black pepper']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Black pepper </p>
                              </li>
                              <li>
                                   <label class="btn-primary bg-none">
                                <div class="surpriseme-box img-check-base surprise">Surprise me! </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Surprise me!" class="hidden" autocomplete="off" required>
                                
                                   </label>
                               
                                
                              </li>
                            </ul>
                            <div class="help-block with-errors step-3-error"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="step-4" class="psychology">
                    <div id="myCarousel" class="carousel fdi-Carousel slide" >
                      <h4 class="h4pd mg-bt20">PSYCHOLOGY</h4>
                      <!-- Carousel items -->
                      <div class="col-md-12 col-sm-12">
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                          <div class="carousel-inner onebyone-carosel">
                            <div class="item active">
                              <div class="row">
                                <div class="item1">
                                  <div class="col-md-4"> <a href="#"><img src="https://www.askiota.com/assets//images/psychology_test/1.jpg" class="img-responsive center-block" id="image1"></a> </div>
                                </div>
                                <div class="item2 item">
                                  <div class="col-md-4"> <a href="#"><img src="https://www.askiota.com/assets//images/psychology_test/2.jpg" class="img-responsive center-block" id="image2"></a> </div>
                                </div>
                                <div class="item3 item">
                                  <div class="col-md-4"> <a href="#"><img src="https://www.askiota.com/assets//images/psychology_test/3.jpg" class="img-responsive center-block" id="image3"></a> </div>
                                </div>
                              </div>
                            </div>
                            <div class="item">
                              <div class="row">
                                <div class="item4">
                                  <div class="col-md-4"> <a href="#"><img src="https://www.askiota.com/assets//images/psychology_test/4.jpg" class="img-responsive center-block" id="image4"></a> </div>
                                </div>
                                <div class="item5">
                                  <div class="col-md-4"> <a href="#"><img src="https://www.askiota.com/assets//images/psychology_test/5.jpg" class="img-responsive center-block" id="image5"></a> </div>
                                </div>
                                <div class="item6">
                                  <div class="col-md-4"> <a href="#"><img src="https://www.askiota.com/assets//images/psychology_test/6.jpg" class="img-responsive center-block" id="image6"></a> </div>
                                </div>
                              </div>
                            </div>
                            <div class="item">
                              <div class="row">
                                <div class="item7">
                                  <div class="col-md-4"> <a href="#"><img src="https://www.askiota.com/assets//images/psychology_test/7.jpg" class="img-responsive center-block" id="image7"></a> </div>
                                </div>
                                <div class="item8">
                                  <div class="col-md-4"> <a href="#"><img src="https://www.askiota.com/assets//images/psychology_test/8.jpg" class="img-responsive center-block" id="image8"></a> </div>
                                </div>
                                <div class="item9">
                                  <div class="col-md-4"> <a href="#"><img src="https://www.askiota.com/assets//images/psychology_test/9.jpg" class="img-responsive center-block" id="image9"></a> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a> <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a> </div>
                          
                         

                          <a href="#myModal_iota_popup" role="button" class="btn btn-default help-btn" data-toggle="modal">Help </a>
                        <!--/carousel-inner--> 
                      </div>
                    </div>
                    <!--/myCarousel--> 
                  </div>
                  <div id="step-5" class="recommend">
                    <div id="form-step-5" role="form" data-toggle="validator">
                      <div class="form-group">
                        <div class="col-md-5 col-sm-5 mg-bt20 col1">
                          <div class="arrow">STARTERS!</div>
                          <ul id="hexGrid">
                            <li class="hex hex-inner"> <a class="hexIn" href="#"> <span class="image-text" id="starter-recomm-1"> Mozarella Cheese </span> <img src="https://www.askiota.com/assets//images/starters/Insieme-starter.png" alt="" />
                              <h1>Price</h1>
                              <p id="starter-recomm-price-1">434</p>
                              </a> </li>
                            <li class="hex hex-inner1"> <a class="hexIn" href="#"> <span class="image-text" id="starter-recomm-2">Rosemary Rubbed </span> <img src="https://www.askiota.com/assets//images/starters/ss1.png" alt="" />
                              <h1> Price</h1>
                              <p id="starter-recomm-price-2">23</p>
                              </a> </li>
                            <li class="hex hex-inner2"> <a class="hexIn" href="#"> <span class="image-text" id="starter-recomm-3"> Adraki Murgh </span> <img src="https://www.askiota.com/assets//images/starters/starter.png" alt="" />
                              <h1>Price</h1>
                              <p id="starter-recomm-price-3">434</p>
                              </a> </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 text-center col1 step-four-iota" >
                          <div id="demo" class="top costummodal-output"></div>
                          <img src="https://www.askiota.com/assets/images/iota_imgl.png"> </div>
                        <div class="col-md-5 col-sm-5 col1 mg-bt20">
                          <div class="arrow-left">MAIN COURSE!</div>
                          <ul id="hexGrid">
                            <li class="hex hex-inner3"> <a class="hexIn" href="#"> <span class="image-text" id="main-recomm-1"> Paneer Jalfrezi</span> <img src="https://www.askiota.com/assets//images/main_course/home_recipe_GaramMasala.png" alt="" />
                              <h1>Price</h1>
                              <p id="main-recomm-price-1">12</p>
                              </a> </li>
                            <li class="hex hex-inner4"> <a class="hexIn" href="#"> <span class="image-text" id="main-recomm-2"> 2. Diwani Handi </span><img src="https://www.askiota.com/assets//images/main_course/kadai-paneer.png" alt="" />
                              <h1>Price</h1>
                              <p id="main-recomm-price-2">3434</p>
                              </a> </li>
                            <li class="hex hex-inner5"> <a class="hexIn" href="#"> <span class="image-text" id="main-recomm-3">Mapo Tofu</span> <img src="https://www.askiota.com/assets//images/main_course/thai_curry_for_web_only.png" alt="" />
                              <h1>Price</h1>
                              <p id="main-recomm-price-3">3434</p>
                              </a> </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </nocache>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="backtotop" style="display: block;"> <a href="#costumModal23" role="button" class="btn btn-default help-btn" data-toggle="modal">Help </a></div>
<div id="costumModal23" class="modal iota" data-easein="perspectiveUpIn" tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-body">
      <div class="col-md-2 pd0"> <img src="assets/images/iota_imgl.png"> </div>
      <div class="col-md-6 pd0">
        <div id="iotamessage" class="left"></div>
      </div>
    </div>
  </div>
</div>
<div class="readonly_label" id="field-function_purpose" style="display:none;">What do your taste buds say today?</div>
<div class="readonly_label1" id="field-function_purpose2" style="display:none;">Crunch. Munch. Chew. Slurp.</div>
<div class="readonly_label2" id="field-function_purpose3" style="display:none;">Which image do you connect with the most?</div>
<div class="readonly_label3" id="field-function_purpose4" style="display:none;">Yummy! Here are some delicious mouth-watering dishes from our kitchen. Bon Appetit.</div>

<div class="modal fade" id="myModal_11" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
             <p>You Can Choose Only Two Ingredients.If You Want To Choose Another One Then Please dismiss previously selected Ingredients.</p>
     
      </div>
    </div>
  </div>
</div>
<script>
var i = 0;
var txt = 'Lets start with something about yourself! ';
var speed = 75;
typeWriter();
function typeWriter() {
  if (i < txt.length) {
    document.getElementById("iotamessage").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }
}
window.onload = function() {
  typeWriter("window onload");
};

</script> 

<script>

document.addEventListener('DOMContentLoaded',function(event){
  // array with texts to type in typewriter
  var dataText = [ "Finding best dishes for you from our kitchen...", "Yummy! Here are some delicious mouth-watering dishes from our kitchen. Bon Appetit."];
  
  // type one text in the typwriter
  // keeps calling itself until the text is finished
  function typeWriter(text, i, fnCallback) {
    // chekc if text isn't finished yet
    if (i < (text.length)) {
      // add next character to h1
     document.querySelector("h6").innerHTML = text.substring(0, i+1) +'<span aria-hidden="true"></span>';

      // wait for a while and call this function again for next character
      setTimeout(function() {
        typeWriter(text, i + 1, fnCallback)
      }, 100);
    }
    // text finished, call callback if there is a callback function
    else if (typeof fnCallback == 'function') {
      // call callback after timeout
      setTimeout(fnCallback, 2000);
    }
  }
  // start a typewriter animation for a text in the dataText array
   function StartTextAnimation(i) {
     if (typeof dataText[i] == 'undefined'){
        setTimeout(function() {
          StartTextAnimation(0);
        }, 20000);
     }
     // check if dataText[i] exists
    if (i < dataText[i].length) {
      // text exists! start typewriter animation
     typeWriter(dataText[i], 0, function(){
       // after callback (and whole text has been animated), start next text
       StartTextAnimation(i + 1);
     });
    }
  }
  // start the text animation
  StartTextAnimation(0);
});
</script>
<p class="align-center footer-content" style="font-size: 14px; line-height: 1.6; "> D-bug Datamines Pvt. Ltd | <span><i class="fa fa-envelope"></i> contact@d-bugdatalabs.com</span></p>

<!-- JS Script --> 
<script src="https://www.askiota.com/assets/js/jquery-2.1.4.min.js"></script> 
<script type="text/javascript" src='https://www.askiota.com/assets/js/rangeslider.js'></script> 
<script src="https://www.askiota.com/assets/js/crum-mega-menu.js"></script> 
<script src="https://www.askiota.com/assets/js/swiper.jquery.min.js"></script> 
<script src="https://www.askiota.com/assets/js/theme-plugins.js"></script> 
<script src="https://www.askiota.com/assets/js/main.js"></script> 
<script src="https://www.askiota.com/assets/js/form-actions.js"></script> 
<script src="https://www.askiota.com/assets/js/velocity.min.js"></script> 
<script src="https://www.askiota.com/assets/js/ScrollMagic.min.js"></script> 
<script src="https://www.askiota.com/assets/js/animation.velocity.min.js"></script> 
<!--<script src="https://www.askiota.com/assets//js/validator/validator.js"></script>--> 
<script src="https://www.askiota.com/assets/js/bootstrap.min.js"></script> 
<script src="https://www.askiota.com/assets/js/validator.min.js"></script> 
<script src="https://www.askiota.com/assets/js/jquery.smartWizard.min.js"></script> 
<script src="https://www.askiota.com/assets/js/jquery.smartWizard.min.js"></script> 
<script src="https://www.askiota.com/assets/js/starratting.js"></script> 
 
    <script>

        $(document).ready(function () {

            filePath = 'https://www.askiota.com/';

            $(".my-rating-9").starRating({
                initialRating: 3.5,
                disableAfterRate: false,
                onHover: function (currentIndex, currentRating, $el) {
                    //$('.live-rating').text(currentIndex);
                    $('#live-rating').val(currentIndex);
                },
                onLeave: function (currentIndex, currentRating, $el) {
                    //$('.live-rating').text(currentRating);
                    $('#live-rating').val(currentIndex);
                }
            });

            $('.img-check').click(function (e) {
                $('.img-check').not(this).removeClass('check')
                        .siblings('input').prop('checked', false);
                $(this).addClass('check')
                        .siblings('input').prop('checked', true);
            });

            $('.img-check-prefernce').click(function (e) {
                $('.img-check-prefernce').not(this).removeClass('check')
                        .siblings('input').prop('checked', false);
                $(this).addClass('check')
                        .siblings('input').prop('checked', true);
            });

            $('.img-check-gender').click(function (e) {
                $('.img-check-gender').not(this).removeClass('check')
                        .siblings('input').prop('checked', false);
                $(this).addClass('check')
                        .siblings('input').prop('checked', true);
            });

            $('.img-check-base').click(function (e) {
                $('.img-check-base').not(this).removeClass('check')
                        .siblings('input').prop('checked', false);
                $(this).addClass('check')
                        .siblings('input').prop('checked', true);
            });

            $('.img-check-ing').click(function (e) {

                var xCheck = $(this).hasClass('check');

                if (xCheck) {
                    $(this).removeClass('check').siblings('input:checkbox').attr('checked', false);
                    $(this).siblings('input:checkbox').val(0);
                } else {
                    $(this).addClass('check').siblings('input:checkbox').attr('checked', true);
                    $(this).siblings('input:checkbox').val(1);
                }

            });

            var btnFinish = $('<button></button>').text('Finish')
                    .addClass('btn btn-info btn-finish disabled')
                    .on('click', function () {
                        if (!$(this).hasClass('disabled')) {
                            var elmForm = $("#myForm");
                            if (elmForm) {
                                elmForm.validator('validate');
                                var elmErr = elmForm.find('.has-error');
                                if (elmErr && elmErr.length > 0) {
                                    alert('Oops we still have error in the form');
                                    return false;
                                } else {
                                    //alert('Great! we are ready to submit form');
                                    $('#myModal').modal('show');
                                    elmForm.submit();
                                    return false;
                                }
                            }
                        }
                    });
            var btnCancel = $('<button></button>').text('Cancel')
                    .addClass('btn btn-danger')
                    .on('click', function () {

                        window.location.href = filePath;
                        return false;

                        //$('#smartwizard').smartWizard("reset"); 
                        //$('#myForm').find("input, textarea").val(""); 
                    });

            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'dots',
                transitionEffect: 'fade',
                showPreviousButton: true,
                toolbarSettings: {toolbarPosition: 'bottom',
                    toolbarExtraButtons: [btnFinish, btnCancel]
                },
                anchorSettings: {
                    markDoneStep: true, // add done css
                    markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                    removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                    enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                }
            });

            $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {

                var elmForm = $("#form-step-" + stepNumber);


                // stepDirection === 'forward' :- this condition allows to do the form validation 
                // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
                if (stepDirection === 'forward' && elmForm) {
                    elmForm.validator('validate');
                    //var elmErr = elmForm.children('.has-error');

                    var elmErr = elmForm.find('.has-error');

                    //alert(elmErr.length);

                    if (elmErr && elmErr.length > 0) {
                        // Form validation failed
                        return false;
                    }
                }
                return true;
            });

            var i = 0;
            var i1 = 0;
            var i2 = 0;
            var i3 = 0;
            var speed = 75;

            function typeWriter1() {

                var txt = $('#field-function_purpose').text();
                if (i < txt.length) {
                    document.getElementById("iotamessage").innerHTML += txt.charAt(i);
                    i++;
                    setTimeout(typeWriter1, speed);
                }
            }
            function typeWriter2() {
                var txt = $('#field-function_purpose2').text();

                if (i1 < txt.length) {
                    document.getElementById("iotamessage").innerHTML += txt.charAt(i1);
                    i1++;
                    setTimeout(typeWriter2, speed);
                }
            }

            function typeWriter3() {


                var txt = $('#field-function_purpose3').text();
                if (i2 < txt.length) {
                    document.getElementById("iotamessage").innerHTML += txt.charAt(i2);
                    i2++;
                    setTimeout(typeWriter3, speed);
                }
            }

            function typeWriter4() {


                var txt = $('#field-function_purpose4').text();
                if (i3 < txt.length) {
                    document.getElementById("demo").innerHTML += txt.charAt(i3);
                    i3++;
                    setTimeout(typeWriter4, speed);
                }
            }

            $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection) {

                //alert(stepNumber);
                if (stepNumber == 1) {
                    var i = 0;
                    var speed = 75;

                    $('#costumModal23').modal('show');

                    setTimeout(function () {
                        $('#costumModal23').modal('hide');
                    }, 6000);

                    $('#iotamessage').empty();
                    typeWriter1();

                }
                if (stepNumber == 2) {

                    var i = 0;
                    var speed = 75;

                    $('#costumModal23').modal('show');

                    setTimeout(function () {
                        $('#costumModal23').modal('hide');
                    }, 6000);

                    $('#iotamessage').empty();
                    typeWriter2();

                }


                if (stepNumber == 3) {
                    
                    
                    var i = 0;
                    var speed = 75;
                    var txt = $('#field-function_purpose3').text();


                    $('#costumModal23').modal('show');

                    setTimeout(function () {
                        $('#costumModal23').modal('hide');
                    }, 6000);

                    $('#iotamessage').empty();
                    typeWriter3();

                    $('.sw-btn-next').attr("disabled", "disabled");
                    $('.sw-btn-prev').attr("disabled", "disabled");
                    //$('.sw-btn-next').attr('disabled');

                    //var $actionBar = $('.actionBar');
                    //$('.buttonNext', $actionBar).addClass('buttonDisabled').off("click");

                    var form = $("#recommForm");

                    var checkboxes = form.find('input[type="checkbox"]');

                    $.each(checkboxes, function (key, value) {

                        $(value).attr('type', 'hidden');
                    });

                    var dataS = form.serialize();

                    // R Call

                    $.ajax({
                        type: "POST",
                        url: filePath + "recommend/r_call",
                        data: dataS,
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                if (data) {

                                    //$('.sw-btn-next').removeClass('disabled');  

                                    $("#main-recomm-1").text(data[1]['Dish']);
                                    $("#main-recomm-price-1").text(data[1]['col2']);

                                    $("#main-recomm-2").text(data[2]['Dish']);
                                    $("#main-recomm-price-2").text(data[2]['col2']);

                                    $("#main-recomm-3").text(data[3]['Dish']);
                                    $("#main-recomm-price-3").text(data[3]['col2']);

                                    $("#starter-recomm-1").text(data[4]['Dish']);
                                    $("#starter-recomm-price-1").text(data[4]['col2']);

                                    $("#starter-recomm-2").text(data[5]['Dish']);
                                    $("#starter-recomm-price-2").text(data[5]['col2']);

                                    $("#starter-recomm-3").text(data[6]['Dish']);
                                    $("#starter-recomm-price-3").text(data[6]['col2']);

                                }
                            }
                        }
                    });


                }

                //alert(stepNumber);
                // Enable finish button only on last step
                if (stepNumber == 4) {
                   $( "li.active" ).prevAll().removeClass("done");
                    var i = 0;
                    var speed = 75;
                    var txt = $('#field-function_purpose4').text();


                    /* $('#costumModal23').modal('show');
                     
                     setTimeout(function () {
                     $('#costumModal23').modal('hide');
                     }, 6000); */

                    $('.help-btn').hide();
                    $('#iotamessage').empty();
                    typeWriter4();

                    $('.sw-btn-next').attr("disabled", "disabled");
                    $('.sw-btn-prev').attr("disabled", "disabled");

                    $('.btn-finish').removeClass('disabled');
                } else {

                    $('.btn-finish').addClass('disabled');
                }

            });

            var val = $("#valR").val();
            $("#range-test").html(val);
            var srcPic = "assets/images/sweet/" + val + ".png";
            $("#img-test").attr("src", srcPic);
            $('#valR').change(function (e) {
                var val = $("#valR").val();
                $("#range-test").html(val);
                var srcPic = "assets/images/sweet/" + val + ".png";
                $("#img-test").attr("src", srcPic);
            });

            var val = $("#valR1").val();
            $("#range-test1").html(val);
            var srcPic = "assets/images/spicy/" + val + ".png";
            $("#img-test1").attr("src", srcPic);
            $('#valR1').change(function (e) {
                var val = $("#valR1").val();
                $("#range-test1").html(val);
                var srcPic = "assets/images/spicy/" + val + ".png";
                $("#img-test1").attr("src", srcPic);
            });

            var val = $("#valR2").val();
            $("#range-test2").html(val);
            var srcPic = "assets/images/cheesy/" + val + ".png";
            $("#img-test2").attr("src", srcPic);
            $('#valR2').change(function (e) {
                var val = $("#valR2").val();
                $("#range-test2").html(val);
                var srcPic = "assets/images/cheesy/" + val + ".png";
                $("#img-test2").attr("src", srcPic);
            });

            var val = $("#valR3").val();
            $("#range-test3").html(val);
            var srcPic = "assets/images/gravy_soupy/" + val + ".png";
            $("#img-test3").attr("src", srcPic);
            $('#valR3').change(function (e) {
                var val = $("#valR3").val();
                $("#range-test3").html(val);
                var srcPic = "assets/images/gravy_soupy/" + val + ".png";
                $("#img-test3").attr("src", srcPic);
            });

            var val = $("#valR4").val();
            $("#range-test4").html(val);
            var srcPic = "assets/images/sour/" + val + ".png";
            $("#img-test4").attr("src", srcPic);
            $('#valR4').change(function (e) {
                var val = $("#valR4").val();
                $("#range-test4").html(val);
                var srcPic = "assets/images/sour/" + val + ".png";
                $("#img-test4").attr("src", srcPic);
            });

            $('#myCarousel').carousel({
                interval: false,
                wrap: false
            })

            $(".item1, .item2, .item3").click(function () {
                $(".right.carousel-control").trigger("click");
            });
            $(".item4, .item5, .item6").click(function () {
                $(".right.carousel-control").trigger("click");
            });
            $(".item7, .item8, .item9").click(function () {

                //next to last step
                $(".sw-btn-next").trigger("click");

            });

            $('.slide').removeClass('active');

            $('input[type="range"]').rangeslider({
                polyfill: false,
                rangeClass: 'rangeslider',
                fillClass: 'rangeslider__fill',
                handleClass: 'rangeslider__handle',
                onInit: function () {

                },
                onSlide: function (position, value) {
                    $('.rangeslider__handle').attr('data-content', value);
                    $('.done').removeClass('active');
                    $('.slide').addClass('active');
                },
                onSlideEnd: function (position, value) {
                    $('.done').addClass('active');
                    $('.slide').removeClass('active');
                }
            });

            $('[data-toggle="tooltip"]').tooltip();
            
            
$('input[type=checkbox]').on('change', function (e) {
    //alert($(this).val());
     $('.surprise').removeClass("check");
     if ($(this).val()!="1") {
         $(this).attr('checked', false);
      $(this).prop('checked', false);
        $(this).removeAttr('checked');
       // $(this).siblings('div').removeClass("check");
        //alert("is checked");
    } else {
         if ($('input[type=checkbox]:checked').length <= 3) {
         $(this).prop('checked',true);
          $(this).attr('checked', true);
           $(this).siblings('div').addClass("check");
        //alert("not checked");
    }
        
    }
      //$(this).prop('checked', true);
    //alert($('input[type=checkbox]:checked').length)
    if ($('input[type=checkbox]:checked').length >= 3) {
       $(this).attr('checked', false);
      $(this).prop('checked', false);
        $(this).removeAttr('checked');
        $(this).siblings('div').removeClass("check");
         $('#myModal_11').modal('show');
      
    }
   
     
    
});

$('.surprise').on('click', function (e) {
   
     $('input[type=checkbox]').attr('checked', false);
      $('input[type=checkbox]').prop('checked', false);
        $('input[type=checkbox]').removeAttr('checked');
        $('input[type=checkbox]').siblings('div').removeClass("check");
      });

        });
        
        function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};
        $(document).ready(function () {
            var images = ['https://www.askiota.com/assets/images/psychology_test/7.jpg', 'https://www.askiota.com/assets/images/psychology_test/8.jpg', 'https://www.askiota.com/assets/images/psychology_test/9.jpg','https://www.askiota.com/assets/images/psychology_test/2.jpg','https://www.askiota.com/assets/images/psychology_test/1.jpg','https://www.askiota.com/assets/images/psychology_test/3.jpg','https://www.askiota.com/assets/images/psychology_test/6.jpg','https://www.askiota.com/assets/images/psychology_test/4.jpg','https://www.askiota.com/assets/images/psychology_test/5.jpg','https://www.askiota.com/assets/images/psychology_test/10.jpg','https://www.askiota.com/assets/images/psychology_test/11.jpg','https://www.askiota.com/assets/images/psychology_test/12.jpg','https://www.askiota.com/assets/images/psychology_test/13.jpg','https://www.askiota.com/assets/images/psychology_test/14.jpg','https://www.askiota.com/assets/images/psychology_test/15.jpg','https://www.askiota.com/assets/images/psychology_test/16.jpg','https://www.askiota.com/assets/images/psychology_test/17.jpg','https://www.askiota.com/assets/images/psychology_test/18.jpg','https://www.askiota.com/assets/images/psychology_test/19.jpg','https://www.askiota.com/assets/images/psychology_test/20.jpg','https://www.askiota.com/assets/images/psychology_test/21.jpg','https://www.askiota.com/assets/images/psychology_test/22.jpg'
            ,'https://www.askiota.com/assets/images/psychology_test/23.jpg','https://www.askiota.com/assets/images/psychology_test/24.jpg','https://www.askiota.com/assets/images/psychology_test/25.png','https://www.askiota.com/assets/images/psychology_test/26.jpg','https://www.askiota.com/assets/images/psychology_test/27.jpg','https://www.askiota.com/assets/images/psychology_test/28.jpg','https://www.askiota.com/assets/images/psychology_test/29.jpg','https://www.askiota.com/assets/images/psychology_test/30.jpg','https://www.askiota.com/assets/images/psychology_test/31.jpg','https://www.askiota.com/assets/images/psychology_test/32.jpg'];
            var images1 = shuffle(images);
            $('#image1').attr('src', images1[0]);
            $('#image2').attr('src', images1[1]);
            $('#image3').attr('src', images1[2]);
            
             $('#image4').attr('src', images1[3]);
            $('#image5').attr('src', images1[4]);
            $('#image6').attr('src', images1[5]);
            
             $('#image7').attr('src', images1[6]);
            $('#image8').attr('src', images1[7]);
            $('#image9').attr('src', images1[8]);
            
          

        });
        

    </script> 
    <script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es,fr,ja', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
        }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> 
 
<!-- ...end JS Script -->
</body></html>
<?php }} ?>
