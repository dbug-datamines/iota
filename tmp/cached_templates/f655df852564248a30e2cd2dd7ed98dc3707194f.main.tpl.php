<?php /*%%SmartyHeaderCode:9008446985b1eb038c14769-56626487%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f655df852564248a30e2cd2dd7ed98dc3707194f' => 
    array (
      0 => '/var/www/html/application/templates/main.tpl',
      1 => 1525329585,
      2 => 'file',
    ),
    '3213d6ad33c00efac827bfbdb12737c01dc2cf83' => 
    array (
      0 => '/var/www/html/application/templates/header.tpl',
      1 => 1528351900,
      2 => 'file',
    ),
    'e6b86816f6ff3eb2dcbac28ac556257ebf1f58d6' => 
    array (
      0 => '/var/www/html/application/templates/message.tpl',
      1 => 1525329585,
      2 => 'file',
    ),
    '3359477b81bb0baf2fce9df6922a5e1e9b90a2db' => 
    array (
      0 => '/var/www/html/application/templates/footer.tpl',
      1 => 1528352422,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9008446985b1eb038c14769-56626487',
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5b20e429a24d84_41909710',
  'has_nocache_code' => true,
  'cache_lifetime' => 3600,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b20e429a24d84_41909710')) {function content_5b20e429a24d84_41909710($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Ask IOTA</title>
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/fonts.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/crumina-fonts.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/normalize.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/grid.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/styles.css">
<!--Plugins styles-->
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/primary-menu.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets/css/bootstrap_modal.css">
<!--Styles for RTL-->
<!--<link rel="stylesheet" type="text/css" href="https://www.askiota.com/assets//css/rtl.css">-->
<!--External fonts-->
<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120477304-1"></script>

    <script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120477304-1');
</script>
 

</head>
<body class=" ">
<!-- Header -->
<header class="header" id="site-header">
  <div class="container">
    <div class="header-content-wrapper">
      <div class="logo"> <a href="https://www.askiota.com/" class="full-block-link"></a> <img src="https://www.askiota.com/assets//images/logo_new.png">
        <div class="logo-text"> 
          <!-- <div class="logo-title">Ask IOTA</div>
          <div class="logo-sub-title">AI Powered Recommendation Engine</div> --> 
        </div>
      </div>
      <div class="pull-right translation" id="google_translate_element"></div>
      <div class="user-menu open-overlay"> <a href="#" class="user-menu-content  js-open-aside"> <span></span> <span></span> <span></span> </a> </div>
    </div>
  </div>
</header>
<!-- ... End Header --> 
<!-- Right-menu --> 

<!-- <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">

                   

                    <div class="modal-content">

                        
                        <div class="modal-body">
                            <span>This application requires access to your location. Please allow the same in your browser settings.</span>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
                        </div>
                    </div>


                </div>
            </div>  -->
<div class="mCustomScrollbar" data-mcs-theme="dark">
  <div class="popup right-menu">
    <div class="right-menu-wrap">
      <div class="user-menu-close js-close-aside"> <a href="#" class="user-menu-content  js-clode-aside"> <span></span> <span></span> </a> </div>
      <div class="logo"> 
        <!--<a href="index.html" class="full-block-link"></a> <img src="img/logo-eye.png" alt="Seosight">-->
        <div class="logo-text">
          <div class="logo-title"><img src="https://www.askiota.com/assets//images/logo_new.png"></div>
        </div>
      </div>
      <p class="text">AI Powered Recommendation Engine</p>
    </div>
    
    <?php if (!$_smarty_tpl->tpl_vars['is_login']->value) {?>
    <?php echo form_open('auth/validate',$_smarty_tpl->tpl_vars['agent_login_form']->value);?>

    <div class="widget login">
      <h4 class="login-title">Sign In to Your Account</h4>
      <span id="error-login" style=" display:none; color: #f15b26;">
      <p>Please enter details.</p>
      </span>
      <input type="hidden" id="latitude-form" value="" name="latitude-form">
      <input type="hidden" id="longitude-form" value="" name="longitude-form">
      <input class="email input-standard-grey" placeholder="Username or Email" type="text" name="uName" required id="uName">
      <input class="password input-standard-grey" placeholder="Password" type="password"  name="uPass" required>
      <div class="login-btn-wrap">
        <div class="btn btn-medium btn--dark btn-hover-shadow" id="agent_register_login_submit" data-loading-text="Processing..."> <span class="text">login now</span> <span class="semicircle"></span> </div>
        <!--<div class="remember-wrap">
          <div class="checkbox">
            <input id="remember" type="checkbox" name="remember" value="remember">
            <label for="remember">Remember Me</label>
          </div>
        </div>--> 
      </div>
      <!--<div class="helped">Lost your password?</div>
      <div class="helped">Register Now</div>--> 
    </div>
    <?php echo form_close();?>

    <?php }?>
    
    <div class="widget contacts">
      <h4 class="contacts-title">Get In Touch</h4>
      <!--<p class="contacts-text">Lorem ipsum dolor sit amet, duis metus ligula amet in purus,
        vitae donec vestibulum enim, tincidunt massa sit, convallis ipsum. </p>--> 
      <!--<div class="contacts-item"> <img src="https://www.askiota.com/assets//img/contact4.png" alt="phone">
        <div class="content"> <a href="#" class="title">+91 9087275584</a>
          <p class="sub-title">Mon-Fri 9am-6pm</p>
        </div>
      </div> -->
      <div class="contacts-item"> <img src="https://www.askiota.com/assets//img/contact5.png" alt="phone">
        <div class="content"> <a href="#" class="title">contact@d-bugdatamines.com</a> 
          <!--<p class="sub-title">online support</p>--> 
        </div>
      </div>
      <div class="contacts-item"> <img src="https://www.askiota.com/assets//img/contact6.png" alt="phone">
        <div class="content"> <a href="#" class="title">D-bug Datamines Pvt. Ltd.</a> 
          <!--<p class="sub-title">Mumbai, India</p>--> 
        </div>
      </div>
      
      <?php if ($_smarty_tpl->tpl_vars['is_login']->value) {?> <a href="<?php echo @constant('BASE_URL');?>
auth/logout" class="btn btn-medium btn--secondary "> <span class="text">Logout</span> <span class="semicircle"></span> </a> <?php }?>
       
      
      <!-- Modal --> 
      
    </div>
  </div>
</div>
<div class="modal fade" id="myModal_error" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p>Please Allow Your Location.</p>
        <p><strong>Note:</strong> This application requires access to your location. Please allow the same in your browser settings.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal_geo" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p>Not Allowed.</p>
        <p><strong>Note:</strong> You are not having permission to access to our application.Try it on our own zone.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ... End Right-menu --> 

<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row bg-border-color medium-padding100 pd-bt15 mg-top-home">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center"> <img src="https://www.askiota.com/assets/img/eat.png" alt="IOTA"  ><span class="success_msg"> <?php if ($_smarty_tpl->tpl_vars['message']->value['status']==true) {?><?php if ($_smarty_tpl->tpl_vars['message']->value['type']=='error_message') {?><a href="javascript:void(0);" class="btn btn-medium btn-border c-primary"><span class="text"><div class="alert alert-danger alert-dismissible fade in" role="alert"><strong>Error!</strong> <?php echo $_smarty_tpl->tpl_vars['message']->value['message'];?>
 </div></span><span class="semicircle"></span></a><?php }?><?php if ($_smarty_tpl->tpl_vars['message']->value['type']=='success_message') {?><a href="javascript:void(0);" class="btn btn-medium btn-border c-primary"><span class="text"><div class="alert alert-success alert-dismissible fade in" role="alert"><strong>Success!</strong> <?php echo $_smarty_tpl->tpl_vars['message']->value['message'];?>
 </div></span><span class="semicircle"></span></a><?php }?><?php }?> </span></div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="heading">
              <h4 class="h1 heading-title">Make Smarter Decisions</h4>
              <div class="heading-line"> <span class="short-line"></span> <span class="long-line"></span> </div>
               
             			  
			  <div class="testimonial-item">
                            <!-- Slider main container -->
                            <div class="swiper-container testimonial__thumb overflow-visible swiper-swiper-unique-id-1 initialized swiper-container-horizontal swiper-container-fade" data-effect="fade" data-loop="false" id="swiper-unique-id-1">

                                <div class="swiper-wrapper" style="transition-duration: 0ms;">
                                    <div class="testimonial-slider-item swiper-slide swiper-slide-prev" style="opacity: 0; transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                                        <div class="testimonial-content">
                                             <p class="heading-text">
			  An Artificial Intelligence powered recommendation engine </p>
											<p class="text" data-swiper-parallax="-200" style="transform: translate3d(-200px, 0px, 0px); transition-duration: 0ms;">
											
											<div id="demo" class="left connent-firstscreen"></div>
                                            </p>
											
											<p class="align-center" style="z-index:99999;">
											<a <?php if ($_smarty_tpl->tpl_vars['is_login']->value) {?> href="<?php echo @constant('BASE_URL');?>
recommend" <?php } else { ?> href="#" <?php }?> class="btn btn-medium btn--secondary <?php if (!$_smarty_tpl->tpl_vars['is_login']->value) {?>user-menu-content js-open-aside<?php }?>"> 
			  <span class="text">Start</span> <span class="semicircle"></span> 
			  </a> 
			  </p>
                                            

                                        </div>
                                        <div class="avatar" data-swiper-parallax="-50" style="transform: translate3d(-50px, 0px, 0px); transition-duration: 0ms;">
                                            <img class="iota-img" src="<?php echo @constant('ASSET_PATH');?>
/images/iota_imgl.png" alt="eat img">
											
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            
                        </div>
				
				
			  	
			  </div>
			
			 
			  
			   
          </div>
		  
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Footer -->

<footer class="footer js-fixed-footer" id="site-footer">
  <div class="sub-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"> <span> &copy; 2018 - D-bug Datamines Pvt Ltd. All Rights Reserved. </span> </div>
      </div>
    </div>
  </div>
</footer>

<!-- JS Script --> 
<script src="https://www.askiota.com/assets/js/jquery-2.1.4.min.js"></script> 
<script src="https://www.askiota.com/assets/js/crum-mega-menu.js"></script> 
<script src="https://www.askiota.com/assets/js/swiper.jquery.min.js"></script> 
<script src="https://www.askiota.com/assets/js/theme-plugins.js"></script> 
<script src="https://www.askiota.com/assets/js/main.js"></script> 
<script src="https://www.askiota.com/assets/js/form-actions.js"></script> 
<script src="https://www.askiota.com/assets/js/velocity.min.js"></script> 
<script src="https://www.askiota.com/assets/js/ScrollMagic.min.js"></script> 
<script src="https://www.askiota.com/assets/js/animation.velocity.min.js"></script> 
<script src="https://www.askiota.com/assets/js/bootstrap_modal.js"></script> 
<script src="https://www.askiota.com/assets/js/validator/validator.js"></script> 
 
<script>

        filePath = 'https://www.askiota.com/';


        function Login(msg) {

            if (msg == "yes")
            {
               
                $("#error-login").hide();

                var proceed = $("#agent_register_login_submit").html();
                var processing = $("#agent_register_login_submit").attr("data-loading-text");

                $("#agent_register_login_submit").attr("disabled", true);
                $("#agent_register_login_submit").text(processing);

                // e.preventDefault();
                var submit = true;

                if (!validator.checkAll($("#agent_login_form"))) {
                    submit = false;
                }

                if (submit) {

                    $('#agent_login_form').submit();

                } else {

                    $("#error-login").show();
                    return false;
                }
            } else {
                
                $('#myModal_geo').modal('show');
                //alert("You are Not having permission to login");
            }

        }

//someFunction();

        function someFunction() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(showLocation, showError);

            } else {
                $('#location').html('Geolocation is not supported by this browser.');
            }

        }

        function showError(error) {

               $('#myModal_error').modal('show');
            //alert("Please update setting part");

            return true;
        }

        function showLocation(position) {

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            
             $('#latitude-form').val(latitude);
              $('#longitude-form').val(longitude);

                $("#error-login").hide();

                var proceed = $("#agent_register_login_submit").html();
                var processing = $("#agent_register_login_submit").attr("data-loading-text");

                $("#agent_register_login_submit").attr("disabled", true);
                $("#agent_register_login_submit").text(processing);

                // e.preventDefault();
                var submit = true;

                if (!validator.checkAll($("#agent_login_form"))) {
                    submit = false;
                }

                if (submit) {

                    $('#agent_login_form').submit();

                } else {

                    $("#error-login").show();
                    return false;
                }


        }

    </script> 
<script>

        $(document).ready(function () {

            var i = 0;
            var txt = 'Hi! I\'m IOTA. Welcome to ask IOTA. Let me help you choose your meal ';
            var speed = 75;

            function typeWriter() {
                if (i < txt.length) {
                    document.getElementById("demo").innerHTML += txt.charAt(i);
                    i++;
                    setTimeout(typeWriter, speed);
                }
            }
            window.onload = function () {
                typeWriter("window onload");
            };


           $("#agent_register_login_submit").click(function (e) {
               
                  var proceed = $("#agent_register_login_submit").html();
                var processing = $("#agent_register_login_submit").attr("data-loading-text");

                $("#agent_register_login_submit").attr("disabled", true);
                $("#agent_register_login_submit").text(processing);
  someFunction();
    }); 

        });
    </script> 
<script>
$(document).ready(function(){
    
    $("#myBtn2").click(function(){
        $("#myModal2").modal({backdrop: false});
    });
   
});
</script> 
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es,fr,ja', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> 
 
<!-- ...end JS Script -->
</body></html><?php }} ?>
