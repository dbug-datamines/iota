<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-06-11 22:54:08
         compiled from "/var/www/html/application/templates/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6205022965b1eb038c51f00-14677363%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3213d6ad33c00efac827bfbdb12737c01dc2cf83' => 
    array (
      0 => '/var/www/html/application/templates/header.tpl',
      1 => 1528351900,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6205022965b1eb038c51f00-14677363',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'is_login' => 1,
    'agent_login_form' => 1,
  ),
  'has_nocache_code' => true,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5b1eb038c8f902_24666680',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b1eb038c8f902_24666680')) {function content_5b1eb038c8f902_24666680($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php echo @constant('SITE_TITLE');?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/fonts.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/crumina-fonts.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/normalize.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/grid.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/styles.css">
<!--Plugins styles-->
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/primary-menu.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/bootstrap_modal.css">
<!--Styles for RTL-->
<!--<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/rtl.css">-->
<!--External fonts-->
<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<?php echo '<script'; ?>
 async src="https://www.googletagmanager.com/gtag/js?id=UA-120477304-1"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120477304-1');
<?php echo '</script'; ?>
>
 

</head>
<body class=" ">
<!-- Header -->
<header class="header" id="site-header">
  <div class="container">
    <div class="header-content-wrapper">
      <div class="logo"> <a href="<?php echo @constant('BASE_URL');?>
" class="full-block-link"></a> <img src="<?php echo @constant('ASSET_PATH');?>
/images/logo_new.png">
        <div class="logo-text"> 
          <!-- <div class="logo-title"><?php echo @constant('SITE_TITLE');?>
</div>
          <div class="logo-sub-title"><?php echo @constant('SUB_SITE_TITLE');?>
</div> --> 
        </div>
      </div>
      <div class="pull-right translation" id="google_translate_element"></div>
      <div class="user-menu open-overlay"> <a href="#" class="user-menu-content  js-open-aside"> <span></span> <span></span> <span></span> </a> </div>
    </div>
  </div>
</header>
<!-- ... End Header --> 
<!-- Right-menu --> 

<!-- <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">

                   

                    <div class="modal-content">

                        
                        <div class="modal-body">
                            <span>This application requires access to your location. Please allow the same in your browser settings.</span>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
                        </div>
                    </div>


                </div>
            </div>  -->
<div class="mCustomScrollbar" data-mcs-theme="dark">
  <div class="popup right-menu">
    <div class="right-menu-wrap">
      <div class="user-menu-close js-close-aside"> <a href="#" class="user-menu-content  js-clode-aside"> <span></span> <span></span> </a> </div>
      <div class="logo"> 
        <!--<a href="index.html" class="full-block-link"></a> <img src="img/logo-eye.png" alt="Seosight">-->
        <div class="logo-text">
          <div class="logo-title"><img src="<?php echo @constant('ASSET_PATH');?>
/images/logo_new.png"></div>
        </div>
      </div>
      <p class="text"><?php echo @constant('SUB_SITE_TITLE');?>
</p>
    </div>
    
    <?php echo '/*%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/<?php if (!$_smarty_tpl->tpl_vars[\'is_login\']->value) {?>/*/%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/';?>

    <?php echo '/*%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/<?php echo form_open(\'auth/validate\',$_smarty_tpl->tpl_vars[\'agent_login_form\']->value);?>
/*/%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/';?>

    <div class="widget login">
      <h4 class="login-title">Sign In to Your Account</h4>
      <span id="error-login" style=" display:none; color: #f15b26;">
      <p>Please enter details.</p>
      </span>
      <input type="hidden" id="latitude-form" value="" name="latitude-form">
      <input type="hidden" id="longitude-form" value="" name="longitude-form">
      <input class="email input-standard-grey" placeholder="Username or Email" type="text" name="uName" required id="uName">
      <input class="password input-standard-grey" placeholder="Password" type="password"  name="uPass" required>
      <div class="login-btn-wrap">
        <div class="btn btn-medium btn--dark btn-hover-shadow" id="agent_register_login_submit" data-loading-text="Processing..."> <span class="text">login now</span> <span class="semicircle"></span> </div>
        <!--<div class="remember-wrap">
          <div class="checkbox">
            <input id="remember" type="checkbox" name="remember" value="remember">
            <label for="remember">Remember Me</label>
          </div>
        </div>--> 
      </div>
      <!--<div class="helped">Lost your password?</div>
      <div class="helped">Register Now</div>--> 
    </div>
    <?php echo '/*%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/<?php echo form_close();?>
/*/%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/';?>

    <?php echo '/*%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/<?php }?>/*/%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/';?>

    
    <div class="widget contacts">
      <h4 class="contacts-title">Get In Touch</h4>
      <!--<p class="contacts-text">Lorem ipsum dolor sit amet, duis metus ligula amet in purus,
        vitae donec vestibulum enim, tincidunt massa sit, convallis ipsum. </p>--> 
      <!--<div class="contacts-item"> <img src="<?php echo @constant('ASSET_PATH');?>
/img/contact4.png" alt="phone">
        <div class="content"> <a href="#" class="title">+91 9087275584</a>
          <p class="sub-title">Mon-Fri 9am-6pm</p>
        </div>
      </div> -->
      <div class="contacts-item"> <img src="<?php echo @constant('ASSET_PATH');?>
/img/contact5.png" alt="phone">
        <div class="content"> <a href="#" class="title">contact@d-bugdatamines.com</a> 
          <!--<p class="sub-title">online support</p>--> 
        </div>
      </div>
      <div class="contacts-item"> <img src="<?php echo @constant('ASSET_PATH');?>
/img/contact6.png" alt="phone">
        <div class="content"> <a href="#" class="title">D-bug Datamines Pvt. Ltd.</a> 
          <!--<p class="sub-title">Mumbai, India</p>--> 
        </div>
      </div>
      
      <?php echo '/*%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/<?php if ($_smarty_tpl->tpl_vars[\'is_login\']->value) {?>/*/%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/';?>
 <a href="<?php echo '/*%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/<?php echo @constant(\'BASE_URL\');?>
/*/%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/';?>
auth/logout" class="btn btn-medium btn--secondary "> <span class="text">Logout</span> <span class="semicircle"></span> </a> <?php echo '/*%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/<?php }?>/*/%%SmartyNocache:6205022965b1eb038c51f00-14677363%%*/';?>

       
      
      <!-- Modal --> 
      
    </div>
  </div>
</div>
<div class="modal fade" id="myModal_error" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p>Please Allow Your Location.</p>
        <p><strong>Note:</strong> This application requires access to your location. Please allow the same in your browser settings.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal_geo" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p>Not Allowed.</p>
        <p><strong>Note:</strong> You are not having permission to access to our application.Try it on our own zone.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ... End Right-menu --> 
<?php }} ?>
