<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-06-13 15:51:25
         compiled from "/var/www/html/application/templates/recommend.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7560054985b1eb09df35f51-64268870%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c8f0e8d42b2158676cdd5fdf361b3dd8ee54b061' => 
    array (
      0 => '/var/www/html/application/templates/recommend.tpl',
      1 => 1528885283,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7560054985b1eb09df35f51-64268870',
  'function' => 
  array (
  ),
  'cache_lifetime' => 3600,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5b1eb09e0577c1_61432832',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b1eb09e0577c1_61432832')) {function content_5b1eb09e0577c1_61432832($_smarty_tpl) {?> <?php echo $_smarty_tpl->getSubTemplate ("header_recomm.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array(), 0);?>
 
<!-- <style>
.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 25px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}
.carousel-inner.onebyone-carosel { margin: auto; width: 80%; }
.onebyone-carosel .active.left { left: -25.00%; }
.onebyone-carosel .active.right { left: 25.00%; }
.onebyone-carosel .next { left: 25.00%; }
.onebyone-carosel .prev { left: -25.00%; }
.left {
  display: none;
}
.right {
  display: none;
}
</style> -->
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row bg-border-color medium-padding70 pd-bt0">
      <div class="container">
        <div class="row">
          <nocache>
            <form action="#" id="recommForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
              <!-- SmartWizard html -->
              <div id="smartwizard">
                <ul>
                  <li><a href="#step-1"><span>Preferences</span> <small></small></a></li>
                  <li><a href="#step-2"><span>Taste</span> <small></small></a></li>
                  <li><a href="#step-3"><span>Base</span> <small></small></a></li>
                  <li><a href="#step-4"><span>Psychology</span> <small> </small></a></li>
                  <li><a href="#step-5"><span>Recommendations</span> <small> </small></a></li>
                </ul>
                <div>
                  <div id="step-1" class="ethnicity">
                    <div id="form-step-0" role="form" data-toggle="validator">
                      <div class="col-md-12 col-sm-12 ">
                        <div class="col-md-6 col-sm-6 col1">
                          <h4>ETHNICITY <span data-toggle="tooltip" data-placement="bottom" title="Ethnicity, noun,
the fact or state of belonging to a social group that has a common national or cultural tradition.
the interrelationship between gender, ethnicity, and class"> <i class="fa fa-question-circle"></i> </span></h4>
                          <div class="form-group position-relative">
                            <ul class="list list--secondary ethnicity_selection">
                              <div class="col-md-12 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">Maharashtrian </a>
                                  <input type="radio" name="chk1" id="item4" value="MAHARASHTRIAN" class="hidden" autocomplete="off" required>
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#"> Gujarati </a>
                                  <input type="radio" name="chk1" id="item4" value="GUJARATI" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#"> South Indian </a>
                                  <input type="radio" name="chk1" id="item4" value="SOUTH INDIAN" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#"> Punjabi </a>
                                  <input type="radio" name="chk1" id="item4" value="PUNJABI" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">Coastal </a>
                                  <input type="radio" name="chk1" id="item4" value="COASTAL" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-12 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">Others </a>
                                  <input type="radio" name="chk1" id="item4" value="OTHERS" class="hidden" autocomplete="off">
                                </li>
                              </div>
                            </ul>
                            <div class="help-block with-errors re-mg-bt"></div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col2">
                          <h4>PREFERENCE </h4>
                          <div class="col-md-12 col-sm-12 border pd0 pd-top10 pd-bottom8 mg-bt10 preference">
                            <div class="form-group radio-new">
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Veg"  autocomplete="off" required>
                                  Veg <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Non-Veg"  autocomplete="off">
                                  Non-Veg <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Jain"  autocomplete="off">
                                  Jain <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5 mg-top10">
                                <label class="radio-check tooltip-label">
                                  <input type="radio" name="chk2" id="item4" value="Vegan"  autocomplete="off">
                                  Vegan <span class="checkmark"></span> </label><span class="vegan" data-toggle="tooltip" data-placement="bottom" title="a person who does not eat or use animal products."> <i class="fa fa-question-circle"></i> </span>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5 mg-top10">
                                <label class="radio-check tooltip-label">
                                  <input type="radio" name="chk2" id="item4" value="Surprise-me"  autocomplete="off">
                                  Surprise me! <span class="checkmark"></span> </label><span class="vegan" data-toggle="tooltip" data-placement="bottom" title="Let IOTA select the one for you basis your other preferences"> <i class="fa fa-question-circle"></i> </span>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5 mg-top10">
                                <label class="radio-check tooltip-label">
                                  <input type="radio" name="chk2" id="item4" value="I'm on DIET"  autocomplete="off">
                                  I'm on DIET <span class="checkmark"></span> </label><span class="vegan" data-toggle="tooltip" data-placement="bottom" title="Low fat, healthy food to meet your health goals"> <i class="fa fa-question-circle"></i> </span>
                              </div>
                              <div class="help-block with-errors "></div>
                            </div>
                          </div>
                          <h4>GENDER </h4>
                          <div class="col-md-12 col-sm-12 border pd0 pd-top10 pd-bottom8 mg-bt10 gender">
                            <div class="form-group radio-new">
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk3" id="item4" value="Male"  autocomplete="off" required>
                                  Male <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk3" id="item4" value="Female" autocomplete="off">
                                  Female<span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-4 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk3" id="item4" value="Others" autocomplete="off">
                                  Others<span class="checkmark"></span> </label>
                              </div>
                              <div class="help-block with-errors"></div>
                            </div>
                          </div>
                          <h4> AGE</h4>
                          <div class="col-md-12 col-sm-12 border pd0 pd-top10 pd-bottom8 mg-bt10 clear">
                            <div class="wrapper-rangeslider ">
                              <input type="range" min="10" max="80" step="1" name="age" value="0"/>
                              <div id="ruler"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="step-2" role="form" data-toggle="validator" class="test">
                    <div id="form-step-1" role="form" data-toggle="validator">
                      <div class="col-md-12 col-sm-12 ">
                        <h4 class="h4pd mg-bt0">TASTE</h4>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block  pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Sweet</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_sweet" id="valR" class="slider" type="range" min="1" max="10" value="0" step="1"/>
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Spicy</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_spicy" id="valR1" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test1" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test1"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Cheesy</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_cheesy" id="valR2" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test2" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test2"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Gravy/Soupy</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_gravy_soupy" id="valR3" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test3" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test3"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Sour</div>
                              <div class="col-md-9 test-slider">
                                <input name="taste_sour" id="valR4" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test4" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test4"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="step-3">
                    <div id="form-step-2" role="form" data-toggle="validator">
                      <div class="col-md-12">
                        <div class="col-md-6 border-right">
                          <div class="form-group position-relative">
                            <h4 class="h4pd text-center">BASE </h4>
                            <ul class="list list--secondary  text-center">
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon1 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Chips/Crispy" class="hidden" autocomplete="off" required>
                                </label>
                                <p class="balloon-title"> Chips/Crispy </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon2 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Noodles" class="hidden" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Noodles 
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon3 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="No Base" class="hidden" autocomplete="off">
                                </label>
                                <p class="balloon-title"> No Base</p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon4 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Rice" class="hidden" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Rice </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon5 img-check-base"> </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Bread" class="hidden" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Bread </p>
                              </li>
                            </ul>
                            <div class="help-block with-errors step-3-error"></div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group position-relative ">
                            <h4 class="h4pd text-center"> INGREDIENTS</h4>
                            <ul class="list list--secondary  text-center">
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon6 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Garlic']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off" required>
                                </label>
                                <p class="balloon-title bg-none"> Garlic </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon7 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Coconut']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Coconut </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon8 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Tomato']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Tomato </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon9 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Mustard']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Mustard </p>
                              </li>
                              <li>
                                <label class="btn-primary bg-none">
                                <div class="balloon balloon10 img-check-ing"></div>
                                <input type="checkbox" name="chk_ing['ing']['Black pepper']" id="chk_ing1" value="0" class="hidden limitThree" autocomplete="off">
                                </label>
                                <p class="balloon-title"> Black pepper </p>
                              </li>
                              <li>
                                   <label class="btn-primary bg-none">
                                <div class="surpriseme-box img-check-base surprise">Surprise me! </div>
                                <input type="radio" name="chk_base" id="chk_base_item1" value="Surprise me!" class="hidden" autocomplete="off" required>
                                
                                   </label>
                               
                                
                              </li>
                            </ul>
                            <div class="help-block with-errors step-3-error"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="step-4" class="psychology">
                    <div id="myCarousel" class="carousel fdi-Carousel slide" >
                      <h4 class="h4pd mg-bt20">PSYCHOLOGY</h4>
                      <!-- Carousel items -->
                      <div class="col-md-12 col-sm-12">
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                          <div class="carousel-inner onebyone-carosel">
                            <div class="item active">
                              <div class="row">
                                <div class="item1">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/1.jpg" class="img-responsive center-block" id="image1"></a> </div>
                                </div>
                                <div class="item2 item">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/2.jpg" class="img-responsive center-block" id="image2"></a> </div>
                                </div>
                                <div class="item3 item">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/3.jpg" class="img-responsive center-block" id="image3"></a> </div>
                                </div>
                              </div>
                            </div>
                            <div class="item">
                              <div class="row">
                                <div class="item4">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/4.jpg" class="img-responsive center-block" id="image4"></a> </div>
                                </div>
                                <div class="item5">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/5.jpg" class="img-responsive center-block" id="image5"></a> </div>
                                </div>
                                <div class="item6">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/6.jpg" class="img-responsive center-block" id="image6"></a> </div>
                                </div>
                              </div>
                            </div>
                            <div class="item">
                              <div class="row">
                                <div class="item7">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/7.jpg" class="img-responsive center-block" id="image7"></a> </div>
                                </div>
                                <div class="item8">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/8.jpg" class="img-responsive center-block" id="image8"></a> </div>
                                </div>
                                <div class="item9">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/9.jpg" class="img-responsive center-block" id="image9"></a> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a> <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a> </div>
                          
                         

                          <a href="#myModal_iota_popup" role="button" class="btn btn-default help-btn" data-toggle="modal">Help </a>
                        <!--/carousel-inner--> 
                      </div>
                    </div>
                    <!--/myCarousel--> 
                  </div>
                  <div id="step-5" class="recommend">
                    <div id="form-step-5" role="form" data-toggle="validator">
                      <div class="form-group">
                        <div class="col-md-5 col-sm-5 mg-bt20 col1">
                          <div class="arrow">STARTERS!</div>
                          <ul id="hexGrid">
                            <li class="hex hex-inner"> <a class="hexIn" href="#"> <span class="image-text" id="starter-recomm-1"> Mozarella Cheese </span> <img src="<?php echo @constant('ASSET_PATH');?>
/images/starters/Insieme-starter.png" alt="" />
                              <h1>Price</h1>
                              <p id="starter-recomm-price-1">434</p>
                              </a> </li>
                            <li class="hex hex-inner1"> <a class="hexIn" href="#"> <span class="image-text" id="starter-recomm-2">Rosemary Rubbed </span> <img src="<?php echo @constant('ASSET_PATH');?>
/images/starters/ss1.png" alt="" />
                              <h1> Price</h1>
                              <p id="starter-recomm-price-2">23</p>
                              </a> </li>
                            <li class="hex hex-inner2"> <a class="hexIn" href="#"> <span class="image-text" id="starter-recomm-3"> Adraki Murgh </span> <img src="<?php echo @constant('ASSET_PATH');?>
/images/starters/starter.png" alt="" />
                              <h1>Price</h1>
                              <p id="starter-recomm-price-3">434</p>
                              </a> </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 text-center col1 step-four-iota" >
                          <div id="demo" class="top costummodal-output"></div>
                          <img src="<?php echo @constant('ASSET_PATH');?>
images/iota_imgl.png"> </div>
                        <div class="col-md-5 col-sm-5 col1 mg-bt20">
                          <div class="arrow-left">MAIN COURSE!</div>
                          <ul id="hexGrid">
                            <li class="hex hex-inner3"> <a class="hexIn" href="#"> <span class="image-text" id="main-recomm-1"> Paneer Jalfrezi</span> <img src="<?php echo @constant('ASSET_PATH');?>
/images/main_course/home_recipe_GaramMasala.png" alt="" />
                              <h1>Price</h1>
                              <p id="main-recomm-price-1">12</p>
                              </a> </li>
                            <li class="hex hex-inner4"> <a class="hexIn" href="#"> <span class="image-text" id="main-recomm-2"> 2. Diwani Handi </span><img src="<?php echo @constant('ASSET_PATH');?>
/images/main_course/kadai-paneer.png" alt="" />
                              <h1>Price</h1>
                              <p id="main-recomm-price-2">3434</p>
                              </a> </li>
                            <li class="hex hex-inner5"> <a class="hexIn" href="#"> <span class="image-text" id="main-recomm-3">Mapo Tofu</span> <img src="<?php echo @constant('ASSET_PATH');?>
/images/main_course/thai_curry_for_web_only.png" alt="" />
                              <h1>Price</h1>
                              <p id="main-recomm-price-3">3434</p>
                              </a> </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </nocache>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="backtotop" style="display: block;"> <a href="#costumModal23" role="button" class="btn btn-default help-btn" data-toggle="modal">Help </a></div>
<div id="costumModal23" class="modal iota" data-easein="perspectiveUpIn" tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-body">
      <div class="col-md-2 pd0"> <img src="assets/images/iota_imgl.png"> </div>
      <div class="col-md-6 pd0">
        <div id="iotamessage" class="left"></div>
      </div>
    </div>
  </div>
</div>
<div class="readonly_label" id="field-function_purpose" style="display:none;">What do your taste buds say today?</div>
<div class="readonly_label1" id="field-function_purpose2" style="display:none;">Crunch. Munch. Chew. Slurp.</div>
<div class="readonly_label2" id="field-function_purpose3" style="display:none;">Which image do you connect with the most?</div>
<div class="readonly_label3" id="field-function_purpose4" style="display:none;">Yummy! Here are some delicious mouth-watering dishes from our kitchen. Bon Appetit.</div>

<div class="modal fade" id="myModal_11" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
             <p>You Can Choose Only Two Ingredients.If You Want To Choose Another One Then Please dismiss previously selected Ingredients.</p>
     
      </div>
    </div>
  </div>
</div>
<?php echo '<script'; ?>
>
var i = 0;
var txt = 'Lets start with something about yourself! ';
var speed = 75;
typeWriter();
function typeWriter() {
  if (i < txt.length) {
    document.getElementById("iotamessage").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }
}
window.onload = function() {
  typeWriter("window onload");
};

<?php echo '</script'; ?>
> 

<?php echo '<script'; ?>
>

document.addEventListener('DOMContentLoaded',function(event){
  // array with texts to type in typewriter
  var dataText = [ "Finding best dishes for you from our kitchen...", "Yummy! Here are some delicious mouth-watering dishes from our kitchen. Bon Appetit."];
  
  // type one text in the typwriter
  // keeps calling itself until the text is finished
  function typeWriter(text, i, fnCallback) {
    // chekc if text isn't finished yet
    if (i < (text.length)) {
      // add next character to h1
     document.querySelector("h6").innerHTML = text.substring(0, i+1) +'<span aria-hidden="true"></span>';

      // wait for a while and call this function again for next character
      setTimeout(function() {
        typeWriter(text, i + 1, fnCallback)
      }, 100);
    }
    // text finished, call callback if there is a callback function
    else if (typeof fnCallback == 'function') {
      // call callback after timeout
      setTimeout(fnCallback, 2000);
    }
  }
  // start a typewriter animation for a text in the dataText array
   function StartTextAnimation(i) {
     if (typeof dataText[i] == 'undefined'){
        setTimeout(function() {
          StartTextAnimation(0);
        }, 20000);
     }
     // check if dataText[i] exists
    if (i < dataText[i].length) {
      // text exists! start typewriter animation
     typeWriter(dataText[i], 0, function(){
       // after callback (and whole text has been animated), start next text
       StartTextAnimation(i + 1);
     });
    }
  }
  // start the text animation
  StartTextAnimation(0);
});
<?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ("footer_recomm.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array(), 0);?>
<?php }} ?>
