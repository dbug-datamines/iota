<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-06-12 17:57:15
         compiled from "/var/www/html/application/templates/footer_recomm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12318311245b1eb09e0958c4-35249661%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '14fe32dc7fc0f1796c14af6b047011c855c96e12' => 
    array (
      0 => '/var/www/html/application/templates/footer_recomm.tpl',
      1 => 1528806002,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12318311245b1eb09e0958c4-35249661',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5b1eb09e0c2653_73330908',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b1eb09e0c2653_73330908')) {function content_5b1eb09e0c2653_73330908($_smarty_tpl) {?><p class="align-center footer-content" style="font-size: 14px; line-height: 1.6; "> D-bug Datamines Pvt. Ltd | <span><i class="fa fa-envelope"></i> contact@d-bugdatalabs.com</span></p>

<!-- JS Script --> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/jquery-2.1.4.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 type="text/javascript" src='<?php echo @constant('ASSET_PATH');?>
js/rangeslider.js'><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/crum-mega-menu.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/swiper.jquery.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/theme-plugins.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/main.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/form-actions.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/velocity.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/ScrollMagic.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/animation.velocity.min.js"><?php echo '</script'; ?>
> 
<!--<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
/js/validator/validator.js"><?php echo '</script'; ?>
>--> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/bootstrap.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/validator.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/jquery.smartWizard.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/jquery.smartWizard.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/starratting.js"><?php echo '</script'; ?>
> 
 
    <?php echo '<script'; ?>
>

        $(document).ready(function () {

            filePath = '<?php echo @constant('BASE_URL');?>
';

            $(".my-rating-9").starRating({
                initialRating: 3.5,
                disableAfterRate: false,
                onHover: function (currentIndex, currentRating, $el) {
                    //$('.live-rating').text(currentIndex);
                    $('#live-rating').val(currentIndex);
                },
                onLeave: function (currentIndex, currentRating, $el) {
                    //$('.live-rating').text(currentRating);
                    $('#live-rating').val(currentIndex);
                }
            });

            $('.img-check').click(function (e) {
                $('.img-check').not(this).removeClass('check')
                        .siblings('input').prop('checked', false);
                $(this).addClass('check')
                        .siblings('input').prop('checked', true);
            });

            $('.img-check-prefernce').click(function (e) {
                $('.img-check-prefernce').not(this).removeClass('check')
                        .siblings('input').prop('checked', false);
                $(this).addClass('check')
                        .siblings('input').prop('checked', true);
            });

            $('.img-check-gender').click(function (e) {
                $('.img-check-gender').not(this).removeClass('check')
                        .siblings('input').prop('checked', false);
                $(this).addClass('check')
                        .siblings('input').prop('checked', true);
            });

            $('.img-check-base').click(function (e) {
                $('.img-check-base').not(this).removeClass('check')
                        .siblings('input').prop('checked', false);
                $(this).addClass('check')
                        .siblings('input').prop('checked', true);
            });

            $('.img-check-ing').click(function (e) {

                var xCheck = $(this).hasClass('check');

                if (xCheck) {
                    $(this).removeClass('check').siblings('input:checkbox').attr('checked', false);
                    $(this).siblings('input:checkbox').val(0);
                } else {
                    $(this).addClass('check').siblings('input:checkbox').attr('checked', true);
                    $(this).siblings('input:checkbox').val(1);
                }

            });

            var btnFinish = $('<button></button>').text('Finish')
                    .addClass('btn btn-info btn-finish disabled')
                    .on('click', function () {
                        if (!$(this).hasClass('disabled')) {
                            var elmForm = $("#myForm");
                            if (elmForm) {
                                elmForm.validator('validate');
                                var elmErr = elmForm.find('.has-error');
                                if (elmErr && elmErr.length > 0) {
                                    alert('Oops we still have error in the form');
                                    return false;
                                } else {
                                    //alert('Great! we are ready to submit form');
                                    $('#myModal').modal('show');
                                    elmForm.submit();
                                    return false;
                                }
                            }
                        }
                    });
            var btnCancel = $('<button></button>').text('Cancel')
                    .addClass('btn btn-danger')
                    .on('click', function () {

                        window.location.href = filePath;
                        return false;

                        //$('#smartwizard').smartWizard("reset"); 
                        //$('#myForm').find("input, textarea").val(""); 
                    });

            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'dots',
                transitionEffect: 'fade',
                showPreviousButton: true,
                toolbarSettings: {toolbarPosition: 'bottom',
                    toolbarExtraButtons: [btnFinish, btnCancel]
                },
                anchorSettings: {
                    markDoneStep: true, // add done css
                    markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                    removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                    enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                }
            });

            $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {

                var elmForm = $("#form-step-" + stepNumber);


                // stepDirection === 'forward' :- this condition allows to do the form validation 
                // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
                if (stepDirection === 'forward' && elmForm) {
                    elmForm.validator('validate');
                    //var elmErr = elmForm.children('.has-error');

                    var elmErr = elmForm.find('.has-error');

                    //alert(elmErr.length);

                    if (elmErr && elmErr.length > 0) {
                        // Form validation failed
                        return false;
                    }
                }
                return true;
            });

            var i = 0;
            var i1 = 0;
            var i2 = 0;
            var i3 = 0;
            var speed = 75;

            function typeWriter1() {

                var txt = $('#field-function_purpose').text();
                if (i < txt.length) {
                    document.getElementById("iotamessage").innerHTML += txt.charAt(i);
                    i++;
                    setTimeout(typeWriter1, speed);
                }
            }
            function typeWriter2() {
                var txt = $('#field-function_purpose2').text();

                if (i1 < txt.length) {
                    document.getElementById("iotamessage").innerHTML += txt.charAt(i1);
                    i1++;
                    setTimeout(typeWriter2, speed);
                }
            }

            function typeWriter3() {


                var txt = $('#field-function_purpose3').text();
                if (i2 < txt.length) {
                    document.getElementById("iotamessage").innerHTML += txt.charAt(i2);
                    i2++;
                    setTimeout(typeWriter3, speed);
                }
            }

            function typeWriter4() {


                var txt = $('#field-function_purpose4').text();
                if (i3 < txt.length) {
                    document.getElementById("demo").innerHTML += txt.charAt(i3);
                    i3++;
                    setTimeout(typeWriter4, speed);
                }
            }

            $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection) {

                //alert(stepNumber);
                if (stepNumber == 1) {
                    var i = 0;
                    var speed = 75;

                    $('#costumModal23').modal('show');

                    setTimeout(function () {
                        $('#costumModal23').modal('hide');
                    }, 6000);

                    $('#iotamessage').empty();
                    typeWriter1();

                }
                if (stepNumber == 2) {

                    var i = 0;
                    var speed = 75;

                    $('#costumModal23').modal('show');

                    setTimeout(function () {
                        $('#costumModal23').modal('hide');
                    }, 6000);

                    $('#iotamessage').empty();
                    typeWriter2();

                }


                if (stepNumber == 3) {
                    
                    
                    var i = 0;
                    var speed = 75;
                    var txt = $('#field-function_purpose3').text();


                    $('#costumModal23').modal('show');

                    setTimeout(function () {
                        $('#costumModal23').modal('hide');
                    }, 6000);

                    $('#iotamessage').empty();
                    typeWriter3();

                    $('.sw-btn-next').attr("disabled", "disabled");
                    $('.sw-btn-prev').attr("disabled", "disabled");
                    //$('.sw-btn-next').attr('disabled');

                    //var $actionBar = $('.actionBar');
                    //$('.buttonNext', $actionBar).addClass('buttonDisabled').off("click");

                    var form = $("#recommForm");

                    var checkboxes = form.find('input[type="checkbox"]');

                    $.each(checkboxes, function (key, value) {

                        $(value).attr('type', 'hidden');
                    });

                    var dataS = form.serialize();

                    // R Call

                    $.ajax({
                        type: "POST",
                        url: filePath + "recommend/r_call",
                        data: dataS,
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                if (data) {

                                    //$('.sw-btn-next').removeClass('disabled');  

                                    $("#main-recomm-1").text(data[1]['Dish']);
                                    $("#main-recomm-price-1").text(data[1]['col2']);

                                    $("#main-recomm-2").text(data[2]['Dish']);
                                    $("#main-recomm-price-2").text(data[2]['col2']);

                                    $("#main-recomm-3").text(data[3]['Dish']);
                                    $("#main-recomm-price-3").text(data[3]['col2']);

                                    $("#starter-recomm-1").text(data[4]['Dish']);
                                    $("#starter-recomm-price-1").text(data[4]['col2']);

                                    $("#starter-recomm-2").text(data[5]['Dish']);
                                    $("#starter-recomm-price-2").text(data[5]['col2']);

                                    $("#starter-recomm-3").text(data[6]['Dish']);
                                    $("#starter-recomm-price-3").text(data[6]['col2']);

                                }
                            }
                        }
                    });


                }

                //alert(stepNumber);
                // Enable finish button only on last step
                if (stepNumber == 4) {
                   $( "li.active" ).prevAll().removeClass("done");
                    var i = 0;
                    var speed = 75;
                    var txt = $('#field-function_purpose4').text();


                    /* $('#costumModal23').modal('show');
                     
                     setTimeout(function () {
                     $('#costumModal23').modal('hide');
                     }, 6000); */

                    $('.help-btn').hide();
                    $('#iotamessage').empty();
                    typeWriter4();

                    $('.sw-btn-next').attr("disabled", "disabled");
                    $('.sw-btn-prev').attr("disabled", "disabled");

                    $('.btn-finish').removeClass('disabled');
                } else {

                    $('.btn-finish').addClass('disabled');
                }

            });

            var val = $("#valR").val();
            $("#range-test").html(val);
            var srcPic = "assets/images/sweet/" + val + ".png";
            $("#img-test").attr("src", srcPic);
            $('#valR').change(function (e) {
                var val = $("#valR").val();
                $("#range-test").html(val);
                var srcPic = "assets/images/sweet/" + val + ".png";
                $("#img-test").attr("src", srcPic);
            });

            var val = $("#valR1").val();
            $("#range-test1").html(val);
            var srcPic = "assets/images/spicy/" + val + ".png";
            $("#img-test1").attr("src", srcPic);
            $('#valR1').change(function (e) {
                var val = $("#valR1").val();
                $("#range-test1").html(val);
                var srcPic = "assets/images/spicy/" + val + ".png";
                $("#img-test1").attr("src", srcPic);
            });

            var val = $("#valR2").val();
            $("#range-test2").html(val);
            var srcPic = "assets/images/cheesy/" + val + ".png";
            $("#img-test2").attr("src", srcPic);
            $('#valR2').change(function (e) {
                var val = $("#valR2").val();
                $("#range-test2").html(val);
                var srcPic = "assets/images/cheesy/" + val + ".png";
                $("#img-test2").attr("src", srcPic);
            });

            var val = $("#valR3").val();
            $("#range-test3").html(val);
            var srcPic = "assets/images/gravy_soupy/" + val + ".png";
            $("#img-test3").attr("src", srcPic);
            $('#valR3').change(function (e) {
                var val = $("#valR3").val();
                $("#range-test3").html(val);
                var srcPic = "assets/images/gravy_soupy/" + val + ".png";
                $("#img-test3").attr("src", srcPic);
            });

            var val = $("#valR4").val();
            $("#range-test4").html(val);
            var srcPic = "assets/images/sour/" + val + ".png";
            $("#img-test4").attr("src", srcPic);
            $('#valR4').change(function (e) {
                var val = $("#valR4").val();
                $("#range-test4").html(val);
                var srcPic = "assets/images/sour/" + val + ".png";
                $("#img-test4").attr("src", srcPic);
            });

            $('#myCarousel').carousel({
                interval: false,
                wrap: false
            })

            $(".item1, .item2, .item3").click(function () {
                $(".right.carousel-control").trigger("click");
            });
            $(".item4, .item5, .item6").click(function () {
                $(".right.carousel-control").trigger("click");
            });
            $(".item7, .item8, .item9").click(function () {

                //next to last step
                $(".sw-btn-next").trigger("click");

            });

            $('.slide').removeClass('active');

            $('input[type="range"]').rangeslider({
                polyfill: false,
                rangeClass: 'rangeslider',
                fillClass: 'rangeslider__fill',
                handleClass: 'rangeslider__handle',
                onInit: function () {

                },
                onSlide: function (position, value) {
                    $('.rangeslider__handle').attr('data-content', value);
                    $('.done').removeClass('active');
                    $('.slide').addClass('active');
                },
                onSlideEnd: function (position, value) {
                    $('.done').addClass('active');
                    $('.slide').removeClass('active');
                }
            });

            $('[data-toggle="tooltip"]').tooltip();
            
            
$('input[type=checkbox]').on('change', function (e) {
    //alert($(this).val());
     $('.surprise').removeClass("check");
     if ($(this).val()!="1") {
         $(this).attr('checked', false);
      $(this).prop('checked', false);
        $(this).removeAttr('checked');
       // $(this).siblings('div').removeClass("check");
        //alert("is checked");
    } else {
         if ($('input[type=checkbox]:checked').length <= 3) {
         $(this).prop('checked',true);
          $(this).attr('checked', true);
           $(this).siblings('div').addClass("check");
        //alert("not checked");
    }
        
    }
      //$(this).prop('checked', true);
    //alert($('input[type=checkbox]:checked').length)
    if ($('input[type=checkbox]:checked').length >= 3) {
       $(this).attr('checked', false);
      $(this).prop('checked', false);
        $(this).removeAttr('checked');
        $(this).siblings('div').removeClass("check");
         $('#myModal_11').modal('show');
      
    }
   
     
    
});

$('.surprise').on('click', function (e) {
   
     $('input[type=checkbox]').attr('checked', false);
      $('input[type=checkbox]').prop('checked', false);
        $('input[type=checkbox]').removeAttr('checked');
        $('input[type=checkbox]').siblings('div').removeClass("check");
      });

        });
        
        function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};
        $(document).ready(function () {
            var images = ['https://www.askiota.com/assets/images/psychology_test/7.jpg', 'https://www.askiota.com/assets/images/psychology_test/8.jpg', 'https://www.askiota.com/assets/images/psychology_test/9.jpg','https://www.askiota.com/assets/images/psychology_test/2.jpg','https://www.askiota.com/assets/images/psychology_test/1.jpg','https://www.askiota.com/assets/images/psychology_test/3.jpg','https://www.askiota.com/assets/images/psychology_test/6.jpg','https://www.askiota.com/assets/images/psychology_test/4.jpg','https://www.askiota.com/assets/images/psychology_test/5.jpg','https://www.askiota.com/assets/images/psychology_test/10.jpg','https://www.askiota.com/assets/images/psychology_test/11.jpg','https://www.askiota.com/assets/images/psychology_test/12.jpg','https://www.askiota.com/assets/images/psychology_test/13.jpg','https://www.askiota.com/assets/images/psychology_test/14.jpg','https://www.askiota.com/assets/images/psychology_test/15.jpg','https://www.askiota.com/assets/images/psychology_test/16.jpg','https://www.askiota.com/assets/images/psychology_test/17.jpg','https://www.askiota.com/assets/images/psychology_test/18.jpg','https://www.askiota.com/assets/images/psychology_test/19.jpg','https://www.askiota.com/assets/images/psychology_test/20.jpg','https://www.askiota.com/assets/images/psychology_test/21.jpg','https://www.askiota.com/assets/images/psychology_test/22.jpg'
            ,'https://www.askiota.com/assets/images/psychology_test/23.jpg','https://www.askiota.com/assets/images/psychology_test/24.jpg','https://www.askiota.com/assets/images/psychology_test/25.png','https://www.askiota.com/assets/images/psychology_test/26.jpg','https://www.askiota.com/assets/images/psychology_test/27.jpg','https://www.askiota.com/assets/images/psychology_test/28.jpg','https://www.askiota.com/assets/images/psychology_test/29.jpg','https://www.askiota.com/assets/images/psychology_test/30.jpg','https://www.askiota.com/assets/images/psychology_test/31.jpg','https://www.askiota.com/assets/images/psychology_test/32.jpg'];
            var images1 = shuffle(images);
            $('#image1').attr('src', images1[0]);
            $('#image2').attr('src', images1[1]);
            $('#image3').attr('src', images1[2]);
            
             $('#image4').attr('src', images1[3]);
            $('#image5').attr('src', images1[4]);
            $('#image6').attr('src', images1[5]);
            
             $('#image7').attr('src', images1[6]);
            $('#image8').attr('src', images1[7]);
            $('#image9').attr('src', images1[8]);
            
          

        });
        

    <?php echo '</script'; ?>
> 
    <?php echo '<script'; ?>
 type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es,fr,ja', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
        }
    <?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"><?php echo '</script'; ?>
> 
 
<!-- ...end JS Script -->
</body></html>
<?php }} ?>
