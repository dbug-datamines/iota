<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-06-12 14:23:04
         compiled from "/var/www/html/application/templates/main1.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4058128775b1eb098a34bb8-75472209%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c3edeabe62d8f1d09afec7e506252441ce0f48aa' => 
    array (
      0 => '/var/www/html/application/templates/main1.tpl',
      1 => 1528793518,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4058128775b1eb098a34bb8-75472209',
  'function' => 
  array (
  ),
  'cache_lifetime' => 3600,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5b1eb098a72ba0_07095276',
  'variables' => 
  array (
    'is_login' => 1,
    'restaurant_name' => 0,
  ),
  'has_nocache_code' => true,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b1eb098a72ba0_07095276')) {function content_5b1eb098a72ba0_07095276($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array(), 0);?>

<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row bg-border-color medium-padding100 pd-bt15 mg-top-home">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center"> <img src="<?php echo @constant('ASSET_PATH');?>
img/eat.png" alt="IOTA"  ><span class="success_msg"> <?php echo $_smarty_tpl->getSubTemplate ("message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array(), 0);?>
 </span></div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="heading">
              <h4 class="h1 heading-title">Make Smarter Decisions</h4>
              <div class="heading-line"> <span class="short-line"></span> <span class="long-line"></span> </div>
              
              <div class="testimonial-item"> 
                <!-- Slider main container -->
                <div class="swiper-container testimonial__thumb overflow-visible swiper-swiper-unique-id-1 initialized swiper-container-horizontal swiper-container-fade" data-effect="fade" data-loop="false" id="swiper-unique-id-1">
                  <div class="swiper-wrapper" style="transition-duration: 0ms;">
                    <div class="testimonial-slider-item swiper-slide swiper-slide-prev" style="opacity: 0; transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                      <div class="testimonial-content">
                        <p class="heading-text"> An Artificial Intelligence powered recommendation engine </p>
                        <p class="text" data-swiper-parallax="-200" style="transform: translate3d(-200px, 0px, 0px); transition-duration: 0ms;">
                        <div id="demo" class="left connent-firstscreen"></div>
                        </p>
                        <p class="align-center" style="z-index:99999;"> <a <?php echo '/*%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/<?php if ($_smarty_tpl->tpl_vars[\'is_login\']->value) {?>/*/%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/';?>
 href="<?php echo '/*%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/<?php echo @constant(\'BASE_URL\');?>
/*/%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/';?>
recommend" <?php echo '/*%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/<?php } else { ?>/*/%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/';?>
 href="#" <?php echo '/*%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/<?php }?>/*/%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/';?>
 class="btn btn-medium btn--secondary <?php echo '/*%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/<?php if (!$_smarty_tpl->tpl_vars[\'is_login\']->value) {?>/*/%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/';?>
user-menu-content js-open-aside<?php echo '/*%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/<?php }?>/*/%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/';?>
"> <span class="text">Start</span> <span class="semicircle"></span> </a> </p>
                      </div>
                      <div class="avatar" data-swiper-parallax="-50" style="transform: translate3d(-50px, 0px, 0px); transition-duration: 0ms;"> <img class="iota-img" src="<?php echo '/*%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/<?php echo @constant(\'ASSET_PATH\');?>
/*/%%SmartyNocache:4058128775b1eb098a34bb8-75472209%%*/';?>
/images/iota_imgl.png" alt="eat img"> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
             </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade welcome-popup" id="myModal_1" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p> Welcome to <img src="<?php echo @constant('ASSET_PATH');?>
/images/logo_new.png"></p>
        <p> Restaurant Name <?php echo $_smarty_tpl->tpl_vars['restaurant_name']->value;?>
</p>
      </div>
    </div>
  </div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array(), 0);?>

 
<?php echo '<script'; ?>
 type="text/javascript">
 var filePath = '<?php echo @constant('BASE_URL');?>
';
 $(window).load(function(){ 
     
   $('#myModal_1').modal('show');
 setTimeout(function() {
     $('#myModal_1').modal('hide');
 }, 3000000000000); 
   
   
    });
 $('#myModal_1').modal({
backdrop: 'static',
 keyboard: false  // to prevent closing with Esc button (if you want this too)
})

 window.setTimeout(function() {
  window.location.href=filePath+"recommend";
}, 5000);

 
 <?php echo '</script'; ?>
> 
<?php }} ?>
