<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-06-13 15:44:13
         compiled from "/var/www/html/application/templates/header_recomm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20999761095b1eb09e05a722-40535639%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'edd1cb73fee0db744b1e53ae1195f20ef697e75c' => 
    array (
      0 => '/var/www/html/application/templates/header_recomm.tpl',
      1 => 1528884849,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20999761095b1eb09e05a722-40535639',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5b1eb09e093b12_53120363',
  'variables' => 
  array (
    'agent_login_form' => 1,
  ),
  'has_nocache_code' => true,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b1eb09e093b12_53120363')) {function content_5b1eb09e093b12_53120363($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php echo @constant('SITE_TITLE');?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/fonts.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/crumina-fonts.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/normalize.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/grid.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/styles.css">
<!--Plugins styles-->
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/primary-menu.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/magnific-popup.css">
<link href="<?php echo @constant('ASSET_PATH');?>
css/hexa.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo @constant('ASSET_PATH');?>
css/bootstrap.min.css">
<link href="<?php echo @constant('ASSET_PATH');?>
css/smart_wizard.css" rel="stylesheet" type="text/css" />

<!-- Optional SmartWizard theme -->
<link href="<?php echo @constant('ASSET_PATH');?>
css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />

<!--Styles for RTL-->
<!--<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/rtl.css">-->
<!--External fonts-->
<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/iota-style.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
css/style.css">
<link rel='stylesheet' href='<?php echo @constant('ASSET_PATH');?>
css/rangeslider.css'>
<link rel='stylesheet' href='<?php echo @constant('ASSET_PATH');?>
css/rangeslider_override.css'>
<link rel="stylesheet" type="text/css" href='<?php echo @constant('ASSET_PATH');?>
css/starratting.css'>
<?php echo '<script'; ?>
 async src="https://www.googletagmanager.com/gtag/js?id=UA-120477304-1"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120477304-1');
<?php echo '</script'; ?>
>
 

</head>
<body class=" ">
 
<!-- Header -->
<header class="header" >
  <div class="container">
    <div class="header-content-wrapper">
      <div class="logo"> <a href="<?php echo '/*%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/<?php echo @constant(\'BASE_URL\');?>
/*/%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/';?>
" class="full-block-link"></a> <img src="<?php echo '/*%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/<?php echo @constant(\'ASSET_PATH\');?>
/*/%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/';?>
/images/logo_new.png">
        <div class="logo-text"> 
          <!--  <div class="logo-title"><?php echo '/*%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/<?php echo @constant(\'SITE_TITLE\');?>
/*/%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/';?>
</div>
                                <div class="logo-sub-title"><?php echo '/*%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/<?php echo @constant(\'SUB_SITE_TITLE\');?>
/*/%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/';?>
</div>--> 
        </div>
      </div>
      <div class="pull-right translation" id="google_translate_element"></div>
    </div>
  </div>
</header>
<div class="modal rating-mobel-box fade" id="myModal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> <?php echo '/*%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/<?php echo form_open(\'auth/add_ratting\',$_smarty_tpl->tpl_vars[\'agent_login_form\']->value);?>
/*/%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/';?>

      <div class="modal-header">
        <h4 class="modal-title">Rate Your Experience</h4>
      </div>
      <div class="modal-body"> <span class="my-rating-9"></span>
        <input type='hidden' name='rating' value="3.5" id='live-rating'>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-lg raised">SUBMIT</button>
        <?php echo '/*%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/<?php echo form_close();?>
/*/%%SmartyNocache:20999761095b1eb09e05a722-40535639%%*/';?>

        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">LATER</button>
      </div>
    </div>
  </div>
</div>
<div class="modal rating-mobel-box fade" id="myModal_geo" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> 
      <div class="modal-body"> <span>You are Not having permission to login</span> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<div class="modal rating-mobel-box fade" id="myModal_error" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> 
      <div class="modal-body"> <span>Please update setting part</span> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<div class="modal myModal_iota_popup fade" id="myModal_iota_popup" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> 
      <div class="modal-header">
        <h4 class="modal-title">Rate Your Experience</h4>
      </div>
     
      <div class="modal-body"> <img src="assets/images/iota_imgl.png"> <div id="" class="left"> <h6>Hallo, Wij zijn Occhio!</h6></div> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
 
<?php }} ?>
