<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-06-11 22:54:08
         compiled from "/var/www/html/application/templates/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20183612765b1eb038ca2224-51845341%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3359477b81bb0baf2fce9df6922a5e1e9b90a2db' => 
    array (
      0 => '/var/www/html/application/templates/footer.tpl',
      1 => 1528352422,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20183612765b1eb038ca2224-51845341',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5b1eb038cbf492_64512366',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b1eb038cbf492_64512366')) {function content_5b1eb038cbf492_64512366($_smarty_tpl) {?><!-- Footer -->

<footer class="footer js-fixed-footer" id="site-footer">
  <div class="sub-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"> <span> <?php echo @constant('COPY');?>
 </span> </div>
      </div>
    </div>
  </div>
</footer>

<!-- JS Script --> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/jquery-2.1.4.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/crum-mega-menu.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/swiper.jquery.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/theme-plugins.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/main.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/form-actions.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/velocity.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/ScrollMagic.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/animation.velocity.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/bootstrap_modal.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo @constant('ASSET_PATH');?>
js/validator/validator.js"><?php echo '</script'; ?>
> 
 
<?php echo '<script'; ?>
>

        filePath = '<?php echo @constant('BASE_URL');?>
';


        function Login(msg) {

            if (msg == "yes")
            {
               
                $("#error-login").hide();

                var proceed = $("#agent_register_login_submit").html();
                var processing = $("#agent_register_login_submit").attr("data-loading-text");

                $("#agent_register_login_submit").attr("disabled", true);
                $("#agent_register_login_submit").text(processing);

                // e.preventDefault();
                var submit = true;

                if (!validator.checkAll($("#agent_login_form"))) {
                    submit = false;
                }

                if (submit) {

                    $('#agent_login_form').submit();

                } else {

                    $("#error-login").show();
                    return false;
                }
            } else {
                
                $('#myModal_geo').modal('show');
                //alert("You are Not having permission to login");
            }

        }

//someFunction();

        function someFunction() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(showLocation, showError);

            } else {
                $('#location').html('Geolocation is not supported by this browser.');
            }

        }

        function showError(error) {

               $('#myModal_error').modal('show');
            //alert("Please update setting part");

            return true;
        }

        function showLocation(position) {

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            
             $('#latitude-form').val(latitude);
              $('#longitude-form').val(longitude);

                $("#error-login").hide();

                var proceed = $("#agent_register_login_submit").html();
                var processing = $("#agent_register_login_submit").attr("data-loading-text");

                $("#agent_register_login_submit").attr("disabled", true);
                $("#agent_register_login_submit").text(processing);

                // e.preventDefault();
                var submit = true;

                if (!validator.checkAll($("#agent_login_form"))) {
                    submit = false;
                }

                if (submit) {

                    $('#agent_login_form').submit();

                } else {

                    $("#error-login").show();
                    return false;
                }


        }

    <?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
>

        $(document).ready(function () {

            var i = 0;
            var txt = 'Hi! I\'m IOTA. Welcome to ask IOTA. Let me help you choose your meal ';
            var speed = 75;

            function typeWriter() {
                if (i < txt.length) {
                    document.getElementById("demo").innerHTML += txt.charAt(i);
                    i++;
                    setTimeout(typeWriter, speed);
                }
            }
            window.onload = function () {
                typeWriter("window onload");
            };


           $("#agent_register_login_submit").click(function (e) {
               
                  var proceed = $("#agent_register_login_submit").html();
                var processing = $("#agent_register_login_submit").attr("data-loading-text");

                $("#agent_register_login_submit").attr("disabled", true);
                $("#agent_register_login_submit").text(processing);
  someFunction();
    }); 

        });
    <?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
>
$(document).ready(function(){
    
    $("#myBtn2").click(function(){
        $("#myModal2").modal({backdrop: false});
    });
   
});
<?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es,fr,ja', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
<?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"><?php echo '</script'; ?>
> 
 
<!-- ...end JS Script -->
</body></html><?php }} ?>
