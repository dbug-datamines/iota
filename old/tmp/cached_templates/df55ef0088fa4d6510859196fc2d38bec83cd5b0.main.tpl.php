<?php /*%%SmartyHeaderCode:15312799805aa5ee3f1db835-19194278%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'df55ef0088fa4d6510859196fc2d38bec83cd5b0' => 
    array (
      0 => '/home/fixbuwio/public_html/IOTA/iota_ci/application/templates/main.tpl',
      1 => 1521292552,
      2 => 'file',
    ),
    '9e2adb77c65a248a6b863bda509f0345d92acd29' => 
    array (
      0 => '/home/fixbuwio/public_html/IOTA/iota_ci/application/templates/header.tpl',
      1 => 1520823453,
      2 => 'file',
    ),
    '7ae4ded7d2d892218da101e8925442093ad9ac68' => 
    array (
      0 => '/home/fixbuwio/public_html/IOTA/iota_ci/application/templates/message.tpl',
      1 => 1520823454,
      2 => 'file',
    ),
    'ca70ba28906ef3918a5ca1a59d85d1a66df56eeb' => 
    array (
      0 => '/home/fixbuwio/public_html/IOTA/iota_ci/application/templates/footer.tpl',
      1 => 1520823453,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15312799805aa5ee3f1db835-19194278',
  'cache_lifetime' => 3600,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5abc95ae187cd0_94429034',
  'has_nocache_code' => true,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abc95ae187cd0_94429034')) {function content_5abc95ae187cd0_94429034($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Ask IOTA</title>
<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/fonts.css">
<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/crumina-fonts.css">
<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/normalize.css">
<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/grid.css">
<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/styles.css">
<!--Plugins styles-->
<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/primary-menu.css">
<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/magnific-popup.css">
<!--Styles for RTL-->
<!--<link rel="stylesheet" type="text/css" href="http://fixbug.in/IOTA/iota_ci/assets//css/rtl.css">-->
<!--External fonts-->
<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
</head>
<body class=" ">
<!-- Header -->
<header class="header" id="site-header">
  <div class="container">
    <div class="header-content-wrapper">
      <div class="logo"> <a href="http://fixbug.in/IOTA/iota_ci/" class="full-block-link"></a> 
        <div class="logo-text">
          <div class="logo-title">Ask IOTA</div>
          <div class="logo-sub-title">AI Powered Recommendation Engine</div>
        </div>
      </div>
            
      <div class="user-menu open-overlay"> <a href="#" class="user-menu-content  js-open-aside"> <span></span> <span></span> <span></span> </a> </div>
    </div>
  </div>
</header>
<!-- ... End Header -->
<!-- Right-menu -->
<div class="mCustomScrollbar" data-mcs-theme="dark">
  <div class="popup right-menu">
    <div class="right-menu-wrap">
      <div class="user-menu-close js-close-aside"> <a href="#" class="user-menu-content  js-clode-aside"> <span></span> <span></span> </a> </div>
      <div class="logo"> <!--<a href="index.html" class="full-block-link"></a> <img src="img/logo-eye.png" alt="Seosight">-->
        <div class="logo-text">
          <div class="logo-title">Ask IOTA</div>
        </div>
      </div>
      <p class="text">AI Powered Recommendation Engine</p>
    </div>
	
	<?php if (!$_smarty_tpl->tpl_vars['is_login']->value) {?>
	<?php echo form_open('auth/validate',$_smarty_tpl->tpl_vars['agent_login_form']->value);?>

    <div class="widget login">
      <h4 class="login-title">Sign In to Your Account</h4>
	  
	  <span id="error-login" style=" display:none; color: #f15b26;"><p>Please enter details.</p></span>
				
      <input class="email input-standard-grey" placeholder="Username or Email" type="text" name="uName" required="required" id="uName">
      <input class="password input-standard-grey" placeholder="Password" type="password"  name="uPass" required="required">
      <div class="login-btn-wrap">
	  
	  
		<div class="btn btn-medium btn--dark btn-hover-shadow" id="agent_register_login_submit"> 
		<span class="text">login now</span> 
		<span class="semicircle"></span> 
		</div>
        <!--<div class="remember-wrap">
          <div class="checkbox">
            <input id="remember" type="checkbox" name="remember" value="remember">
            <label for="remember">Remember Me</label>
          </div>
        </div>-->
      </div>
      <!--<div class="helped">Lost your password?</div>
      <div class="helped">Register Now</div>-->
    </div>
	<?php echo form_close();?>

	<?php }?>
    
    <div class="widget contacts">
      <h4 class="contacts-title">Get In Touch</h4>
      <!--<p class="contacts-text">Lorem ipsum dolor sit amet, duis metus ligula amet in purus,
        vitae donec vestibulum enim, tincidunt massa sit, convallis ipsum. </p>-->
      <div class="contacts-item"> <img src="http://fixbug.in/IOTA/iota_ci/assets//img/contact4.png" alt="phone">
        <div class="content"> <a href="#" class="title">+91 9087275584</a>
          <p class="sub-title">Mon-Fri 9am-6pm</p>
        </div>
      </div>
      <div class="contacts-item"> <img src="http://fixbug.in/IOTA/iota_ci/assets//img/contact5.png" alt="phone">
        <div class="content"> <a href="#" class="title">contact@d-bugdatalabs.com</a>
          <!--<p class="sub-title">online support</p>-->
        </div>
      </div>
      <div class="contacts-item"> <img src="http://fixbug.in/IOTA/iota_ci/assets//img/contact6.png" alt="phone">
        <div class="content"> <a href="#" class="title">D-bug Datamines Pvt. Ltd.</a>
          <!--<p class="sub-title">Mumbai, India</p>-->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ... End Right-menu -->


<div class="content-wrapper">




<div class="container-fluid">
    <div class="row bg-border-color medium-padding100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <img src="http://fixbug.in/IOTA/iota_ci/assets/img/eat.png" alt="IOTA"  >
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="heading">
                        <h4 class="h1 heading-title">Make Smarter Decisions</h4>
                        <div class="heading-line">
                            <span class="short-line"></span>
                            <span class="long-line"></span>
                        </div>
						<?php if ($_smarty_tpl->tpl_vars['message']->value['status']==true) {?><?php if ($_smarty_tpl->tpl_vars['message']->value['type']=='error_message') {?><div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span> </button><strong>Error!</strong> <?php echo $_smarty_tpl->tpl_vars['message']->value['message'];?>
 </div><?php }?><?php if ($_smarty_tpl->tpl_vars['message']->value['type']=='success_message') {?><div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span> </button><strong>Success!</strong> <?php echo $_smarty_tpl->tpl_vars['message']->value['message'];?>
 </div><?php }?><?php }?>
                        <p class="heading-text">An Artificial Intelligence powered recommendation engine
                        </p>
						
						<a <?php if ($_smarty_tpl->tpl_vars['is_login']->value) {?> href="<?php echo @constant('BASE_URL');?>
recommend" <?php } else { ?> href="#" <?php }?> class="btn btn-medium btn--secondary <?php if (!$_smarty_tpl->tpl_vars['is_login']->value) {?>user-menu-content js-open-aside<?php }?>">
							<span class="text">Start</span>
							<span class="semicircle"></span>
						</a>
						
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>


</div>

<!-- Footer -->

<footer class="footer js-fixed-footer" id="site-footer">
  <div class="sub-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <span> &copy; 2018 - D-bug Datamines Pvt Ltd. All Rights Reserved. </span> 
		
		  
		  </div>
      </div>
    </div>
  </div>
</footer>
<!-- JS Script -->
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/jquery-2.1.4.min.js"></script>
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/crum-mega-menu.js"></script>
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/swiper.jquery.min.js"></script>
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/theme-plugins.js"></script>
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/main.js"></script>
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/form-actions.js"></script>
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/velocity.min.js"></script>
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/ScrollMagic.min.js"></script>
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/animation.velocity.min.js"></script>
<script src="http://fixbug.in/IOTA/iota_ci/assets//js/validator/validator.js"></script>


<script>
$(document).ready(function () {

$("#agent_register_login_submit").click(function (e) {

		$("#error-login").hide();
		
        e.preventDefault();
        var submit = true;

        if (!validator.checkAll($("#agent_login_form"))) {
            submit = false;
        }

        if (submit) {
			//alert(1);
            $('#agent_login_form').submit();
        } else {
			//alert(2);
			$("#error-login").show();
			
            return false;
        }
    }); 

});
</script>

<!-- ...end JS Script -->
</body></html><?php }} ?>
