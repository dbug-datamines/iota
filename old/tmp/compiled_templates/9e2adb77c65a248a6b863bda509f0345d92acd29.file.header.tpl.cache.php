<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-03-12 03:04:31
         compiled from "/home/fixbuwio/public_html/IOTA/iota_ci/application/templates/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15866619605aa5ee3f8f7939-46401996%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9e2adb77c65a248a6b863bda509f0345d92acd29' => 
    array (
      0 => '/home/fixbuwio/public_html/IOTA/iota_ci/application/templates/header.tpl',
      1 => 1520823453,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15866619605aa5ee3f8f7939-46401996',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'is_login' => 1,
    'agent_login_form' => 1,
  ),
  'has_nocache_code' => true,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5aa5ee3f9b9758_62633901',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5aa5ee3f9b9758_62633901')) {function content_5aa5ee3f9b9758_62633901($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php echo @constant('SITE_TITLE');?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/fonts.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/crumina-fonts.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/normalize.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/grid.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/styles.css">
<!--Plugins styles-->
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/primary-menu.css">
<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/magnific-popup.css">
<!--Styles for RTL-->
<!--<link rel="stylesheet" type="text/css" href="<?php echo @constant('ASSET_PATH');?>
/css/rtl.css">-->
<!--External fonts-->
<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
</head>
<body class=" ">
<!-- Header -->
<header class="header" id="site-header">
  <div class="container">
    <div class="header-content-wrapper">
      <div class="logo"> <a href="<?php echo @constant('BASE_URL');?>
" class="full-block-link"></a> 
        <div class="logo-text">
          <div class="logo-title"><?php echo @constant('SITE_TITLE');?>
</div>
          <div class="logo-sub-title"><?php echo @constant('SUB_SITE_TITLE');?>
</div>
        </div>
      </div>
            
      <div class="user-menu open-overlay"> <a href="#" class="user-menu-content  js-open-aside"> <span></span> <span></span> <span></span> </a> </div>
    </div>
  </div>
</header>
<!-- ... End Header -->
<!-- Right-menu -->
<div class="mCustomScrollbar" data-mcs-theme="dark">
  <div class="popup right-menu">
    <div class="right-menu-wrap">
      <div class="user-menu-close js-close-aside"> <a href="#" class="user-menu-content  js-clode-aside"> <span></span> <span></span> </a> </div>
      <div class="logo"> <!--<a href="index.html" class="full-block-link"></a> <img src="img/logo-eye.png" alt="Seosight">-->
        <div class="logo-text">
          <div class="logo-title"><?php echo @constant('SITE_TITLE');?>
</div>
        </div>
      </div>
      <p class="text"><?php echo @constant('SUB_SITE_TITLE');?>
</p>
    </div>
	
	<?php echo '/*%%SmartyNocache:15866619605aa5ee3f8f7939-46401996%%*/<?php if (!$_smarty_tpl->tpl_vars[\'is_login\']->value) {?>/*/%%SmartyNocache:15866619605aa5ee3f8f7939-46401996%%*/';?>

	<?php echo '/*%%SmartyNocache:15866619605aa5ee3f8f7939-46401996%%*/<?php echo form_open(\'auth/validate\',$_smarty_tpl->tpl_vars[\'agent_login_form\']->value);?>
/*/%%SmartyNocache:15866619605aa5ee3f8f7939-46401996%%*/';?>

    <div class="widget login">
      <h4 class="login-title">Sign In to Your Account</h4>
	  
	  <span id="error-login" style=" display:none; color: #f15b26;"><p>Please enter details.</p></span>
				
      <input class="email input-standard-grey" placeholder="Username or Email" type="text" name="uName" required="required" id="uName">
      <input class="password input-standard-grey" placeholder="Password" type="password"  name="uPass" required="required">
      <div class="login-btn-wrap">
	  
	  
		<div class="btn btn-medium btn--dark btn-hover-shadow" id="agent_register_login_submit"> 
		<span class="text">login now</span> 
		<span class="semicircle"></span> 
		</div>
        <!--<div class="remember-wrap">
          <div class="checkbox">
            <input id="remember" type="checkbox" name="remember" value="remember">
            <label for="remember">Remember Me</label>
          </div>
        </div>-->
      </div>
      <!--<div class="helped">Lost your password?</div>
      <div class="helped">Register Now</div>-->
    </div>
	<?php echo '/*%%SmartyNocache:15866619605aa5ee3f8f7939-46401996%%*/<?php echo form_close();?>
/*/%%SmartyNocache:15866619605aa5ee3f8f7939-46401996%%*/';?>

	<?php echo '/*%%SmartyNocache:15866619605aa5ee3f8f7939-46401996%%*/<?php }?>/*/%%SmartyNocache:15866619605aa5ee3f8f7939-46401996%%*/';?>

    
    <div class="widget contacts">
      <h4 class="contacts-title">Get In Touch</h4>
      <!--<p class="contacts-text">Lorem ipsum dolor sit amet, duis metus ligula amet in purus,
        vitae donec vestibulum enim, tincidunt massa sit, convallis ipsum. </p>-->
      <div class="contacts-item"> <img src="<?php echo @constant('ASSET_PATH');?>
/img/contact4.png" alt="phone">
        <div class="content"> <a href="#" class="title">+91 9087275584</a>
          <p class="sub-title">Mon-Fri 9am-6pm</p>
        </div>
      </div>
      <div class="contacts-item"> <img src="<?php echo @constant('ASSET_PATH');?>
/img/contact5.png" alt="phone">
        <div class="content"> <a href="#" class="title">contact@d-bugdatalabs.com</a>
          <!--<p class="sub-title">online support</p>-->
        </div>
      </div>
      <div class="contacts-item"> <img src="<?php echo @constant('ASSET_PATH');?>
/img/contact6.png" alt="phone">
        <div class="content"> <a href="#" class="title">D-bug Datamines Pvt. Ltd.</a>
          <!--<p class="sub-title">Mumbai, India</p>-->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ... End Right-menu -->
<?php }} ?>
