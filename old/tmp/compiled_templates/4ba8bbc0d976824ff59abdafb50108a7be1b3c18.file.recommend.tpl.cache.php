<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-03-17 14:07:33
         compiled from "/home/fixbuwio/public_html/IOTA/iota_ci/application/templates/recommend.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3940471225aa5ee525a2292-73058340%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ba8bbc0d976824ff59abdafb50108a7be1b3c18' => 
    array (
      0 => '/home/fixbuwio/public_html/IOTA/iota_ci/application/templates/recommend.tpl',
      1 => 1521294565,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3940471225aa5ee525a2292-73058340',
  'function' => 
  array (
  ),
  'cache_lifetime' => 3600,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5aa5ee52651d25_11557993',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5aa5ee52651d25_11557993')) {function content_5aa5ee52651d25_11557993($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header_recomm.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array(), 0);?>
 
<!-- <style>
.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 25px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}
.carousel-inner.onebyone-carosel { margin: auto; width: 80%; }
.onebyone-carosel .active.left { left: -25.00%; }
.onebyone-carosel .active.right { left: 25.00%; }
.onebyone-carosel .next { left: 25.00%; }
.onebyone-carosel .prev { left: -25.00%; }
.left {
  display: none;
}
.right {
  display: none;
}
</style> -->
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row bg-border-color" style="padding:10px; 0;">
      <div class="container">
        <div class="row">
          <nocache>
            <form action="#" id="myForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
              
              <!-- SmartWizard html -->
              <div id="smartwizard">
                <ul>
                  <li><a href="#step-1">Step 1<br />
                    <small>Preferences</small></a></li>
                  <li><a href="#step-2">Step 2<br />
                    <small>Taste</small></a></li>
                  <li><a href="#step-3">Step 3<br />
                    <small>Base</small></a></li>
                  <li><a href="#step-4">Step 4<br />
                    <small>Psychology </small></a></li>
                  <li><a href="#step-5">Step 5<br />
                    <small>Recommendations </small></a></li>
                </ul>
                <div>
                  <div id="step-1" class="ethnicity">
                    <div id="form-step-0" role="form" data-toggle="validator">
                      <div class="col-md-12 col-sm-12 ">
                        <div class="col-md-6 col-sm-6 col1">
                          <h4>ETHNICITY <span  data-toggle="tooltip" data-placement="right" title="ethnicity, noun,
the fact or state of belonging to a social group that has a common national or cultural tradition.
the interrelationship between gender, ethnicity, and class"> <i class="fa fa-question-circle"></i> </h4>
                          <div class="form-group">
                            <ul class="list list--secondary ethnicity_selection">
                              <div class="col-md-12 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">MAHARASHTRIAN </a>
                                  <input type="radio" name="chk1" id="item4" value="MAHARASHTRIAN" class="hidden" autocomplete="off" required>
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">GUJARATI </a>
                                  <input type="radio" name="chk1" id="item4" value="GUJARATI" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">SOUTH INDIAN </a>
                                  <input type="radio" name="chk1" id="item4" value="SOUTH INDIAN" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">PUNJABI </a>
                                  <input type="radio" name="chk1" id="item4" value="PUNJABI" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-6 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">COASTAL </a>
                                  <input type="radio" name="chk1" id="item4" value="COASTAL" class="hidden" autocomplete="off">
                                </li>
                              </div>
                              <div class="col-md-12 pd0">
                                <li> <a class="img-check hvr-rectangle-out"  href="#">OTHERS </a>
                                  <input type="radio" name="chk1" id="item4" value="OTHERS" class="hidden" autocomplete="off">
                                </li>
                              </div>
                            </ul>
                            <div class="help-block with-errors"></div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col2">
                          <h4>PREFERENCE </h4>
                          <div class="col-md-12 col-sm-12 border pd0 pd-top10 pd-bottom8 mg-bt10 preference">
                            <div class="form-group radio-new">
                              <div class="col-md-3 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Veg"  autocomplete="off" required>
                                  Veg <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-3 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Non-Veg"  autocomplete="off">
                                  Non-Veg <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-3 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Jain"  autocomplete="off">
                                  Jain <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-3 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk2" id="item4" value="Vegan"  autocomplete="off">
                                  Vegan <span class="checkmark"></span> </label>
                              </div>
                              <div class="help-block with-errors"></div>
                            </div>
                          </div>
                          <h4>Gender </h4>
                          <div class="col-md-12 col-sm-12 border pd0 pd-top10 pd-bottom8 mg-bt10 gender">
                            <div class="form-group radio-new">
                              <div class="col-md-3 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk3" id="item4" value="Male"  autocomplete="off" required>
                                  Male <span class="checkmark"></span> </label>
                              </div>
                              <div class="col-md-3 pd-lt5 pd-rt5">
                                <label class="radio-check">
                                  <input type="radio" name="chk3" id="item4" value="Female" autocomplete="off">
                                  Female<span class="checkmark"></span> </label>
                              </div>
                              </li>
                              </ul>
                              <div class="help-block with-errors"></div>
                            </div>
                          </div>
                          <h4>Age</h4>
                          <div class="col-md-12 col-sm-12 border pd0 pd-top10 pd-bottom8 mg-bt10 clear">
                            <div class="wrapper-rangeslider ">
                              <input type="range" min="10" max="80" step="1" name="age" value="0"/>
                              <div id="ruler"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="step-2" role="form" data-toggle="validator" class="test">
                    <div id="form-step-1" role="form" data-toggle="validator">
                      <div class="col-md-12 col-sm-12 ">
                        <h4 class="h4pd mg-bt0">TASTE</h4>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block  pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Sweet</div>
                              <div class="col-md-9 test-slider">
                                <input id="valR" class="slider" type="range" min="1" max="10" value="0" step="1"/>
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Spicy</div>
                              <div class="col-md-9 test-slider">
                                <input id="valR1" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test1" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test1"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Cheesy</div>
                              <div class="col-md-9 test-slider">
                                <input id="valR2" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test2" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test2"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Gravy/Soupy</div>
                              <div class="col-md-9 test-slider">
                                <input id="valR3" class="slider" type="range" min="1" max="10" value="0" step="1" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test3" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test3"> </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col1 mg-bt20">
                          <div class="border test-inner">
                            <div class="col-md-10 col-sm-10 test-inner-block pd0 pd-top15">
                              <div class="col-md-3 pd0 testtitle">Sour</div>
                              <div class="col-md-9 test-slider">
                                <input id="valR4" class="slider" type="range" min="1" max="10" value="0" step="1" oninput="showVal4(this.value)" onChange="showVal4(this.value)" />
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 test-img"> <span  id="range-test4" class="hide-range">0</span>
                              <div class="test-img-div"> <img id="img-test4"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="step-3" class="base">
                    <div id="form-step-2" role="form" data-toggle="validator">
                      <div class="form-group">
                        <div class="col-md-12">
                          <h4 class="h4pd">BASE</h4>
                          <ul class="list list--secondary  text-center">
                            <li>
                              <div class=" box">
                                <label class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/nachos.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item1" value="Chips/Crispy" class="hidden" autocomplete="off" required>
                               
                                </label>
                                 <span>  Chips/Crispy</span>
                                 </div>
                            </li>
                            <li>
                              <div class=" box">
                                <label  class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/Noodles-PNG-Image.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item2" value="Noodles" class="hidden" autocomplete="off">
                                </label>
                                Noodles </div>
                            </li>
                            <li>
                              <div class=" box">
                                <label class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/paneer-butter-masala.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item3" value="No Base" class="hidden" autocomplete="off">
                                </label>
                                No Base </div>
                            </li>
                            <li>
                              <div class=" box">
                                <label class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/plate.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item4" value="Rice" class="hidden" autocomplete="off">
                                </label>
                                Rice </div>
                            </li>
                            <li>
                              <div class="box">
                                <label class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/Bread-PNG-Pic.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item5" value="Bread" class="hidden" autocomplete="off">
                                </label>
                                Bread </div>
                            </li>
                          
                            <li>
                              <div class=" box">
                                <label class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/nachos.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item1" value="Chips/Crispy" class="hidden" autocomplete="off" required>
                                </label>
                                Chips/Crispy </div>
                            </li>
                            <li>
                              <div class=" box">
                                <label class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/Noodles-PNG-Image.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item2" value="Noodles" class="hidden" autocomplete="off">
                                </label>
                                Noodles </div>
                            </li>
                            <li>
                              <div class=" box">
                                <label class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/paneer-butter-masala.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item3" value="No Base" class="hidden" autocomplete="off">
                                </label>
                                No Base </div>
                            </li>
                            <li>
                              <div class=" box">
                                <label class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/plate.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item4" value="Rice" class="hidden" autocomplete="off">
                                </label>
                                Rice </div>
                            </li>
                            <li>
                              <div class="box">
                                <label class="btn-primary"> <img src="<?php echo @constant('ASSET_PATH');?>
/images/base/Bread-PNG-Pic.png" alt="..." class="img-thumbnail img-check-prefernce">
                                  <input type="radio" name="chk_base" id="chk_base_item5" value="Bread" class="hidden" autocomplete="off">
                                </label>
                                Bread </div>
                            </li>
                          </ul>
                          <div class="help-block with-errors"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="step-4" class="psychology">
                    <div id="myCarousel" class="carousel fdi-Carousel slide" >
                      <h4 class="h4pd mg-bt20">Psychology</h4>
                      <!-- Carousel items -->
                      <div class="col-md-12 col-sm-12">
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                          <div class="carousel-inner onebyone-carosel">
                            <div class="item active">
                              <div class="row">
                                <div class="item1">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/1.jpg" class="img-responsive center-block"></a> </div>
                                </div>
                                <div class="item2 item">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/2.jpg" class="img-responsive center-block"></a> </div>
                                </div>
                                <div class="item3 item">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/3.jpg" class="img-responsive center-block"></a> </div>
                                </div>
                              </div>
                            </div>
                            <div class="item">
                              <div class="row">
                                <div class="item4">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/4.jpg" class="img-responsive center-block"></a> </div>
                                </div>
                                <div class="item5">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/5.jpg" class="img-responsive center-block"></a> </div>
                                </div>
                                <div class="item6">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/6.jpg" class="img-responsive center-block"></a> </div>
                                </div>
                              </div>
                            </div>
                            <div class="item">
                              <div class="row">
                                <div class="item7">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/7.jpg" class="img-responsive center-block"></a> </div>
                                </div>
                                <div class="item8">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/8.jpg" class="img-responsive center-block"></a> </div>
                                </div>
                                <div class="item9">
                                  <div class="col-md-4"> <a href="#"><img src="<?php echo @constant('ASSET_PATH');?>
/images/psychology_test/9.jpg" class="img-responsive center-block"></a> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a> <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a> </div>
                        <!--/carousel-inner--> 
                      </div>
                    </div>
                    <!--/myCarousel--> 
                    
                  </div>
                  <div id="step-5" class="recommend">
                    <div id="form-step-5" role="form" data-toggle="validator">
                      <div class="form-group">
                        <div class="col-md-5 col-sm-5 mg-bt20 col1">
                          <div class="arrow">STARTERS!</div>
                          <ul id="hexGrid">
                            <li class="hex hex-inner"> <a class="hexIn" href="#"><span class="image-text"> 1. Mozarella Cheese<br />
                              Fingers</span><img src="<?php echo @constant('ASSET_PATH');?>
/images/starters/Insieme-starter.png" alt="" />
                              <h1>Price</h1>
                              <p>434</p>
                              </a> </li>
                            <li class="hex hex-inner1"> <a class="hexIn" href="#"><span class="image-text"> 2. Rosemary Rubbed<br />
                              grilled chicken</span> <img src="<?php echo @constant('ASSET_PATH');?>
/images/starters/ss1.png" alt="" />
                              <h1> Price</h1>
                              <p>23</p>
                              </a> </li>
                            <li class="hex hex-inner2"> <a class="hexIn" href="#"> <span class="image-text"> 3. Adraki Murgh <br />
                              Shorba</span><img src="<?php echo @constant('ASSET_PATH');?>
/images/starters/starter.png" alt="" />
                              <h1>Price</h1>
                              <p>434</p>
                              </a> </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 text-center col1" style="padding-top: 170px;"> <div id="demo" class="top costummodal-output"></div>  <img src="<?php echo @constant('ASSET_PATH');?>
images/iota_img3.png"> </div>
                        <div class="col-md-5 col-sm-5 col1 mg-bt20">
                          <div class="arrow-left">MAIN COURSE!</div>
                          <ul id="hexGrid">
                            <li class="hex hex-inner3"> <a class="hexIn" href="#"> <span class="image-text"> 1. Paneer Jalfrezi</span><img src="<?php echo @constant('ASSET_PATH');?>
/images/main_course/home_recipe_GaramMasala.png" alt="" />
                              <h1>Price</h1>
                              <p>12</p>
                              </a> </li>
                            <li class="hex hex-inner4"> <a class="hexIn" href="#"> <span class="image-text"> 2. Diwani Handi </span><img src="<?php echo @constant('ASSET_PATH');?>
/images/main_course/kadai-paneer.png" alt="" />
                              <h1>Price</h1>
                              <p>3434</p>
                              </a> </li>
                            <li class="hex hex-inner5"> <a class="hexIn" href="#"> <span class="image-text">3. Mapo Tofu</span><img src="<?php echo @constant('ASSET_PATH');?>
/images/main_course/thai_curry_for_web_only.png" alt="" />
                              <h1>Price</h1>
                              <p>3434</p>
                              </a> </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
            </form>
          </nocache>
        </div>
      </div>
    </div>
  </div>
</div>
 

<?php echo $_smarty_tpl->getSubTemplate ("footer_recomm.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array(), 0);?>
<?php }} ?>
