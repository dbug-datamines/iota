<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function index() {
		
		$message['status'] = false;

        if ($this->session->flashdata('message')) {
            $message = $this->session->flashdata('message');
        }
        $this->template->assign('message', $message);

		//If user is not login show him the login page
        if (!$this->session->userdata('is_logged_in_agent')) {
            redirect();
        } 
    }
	
	//Login verification
    public function validate() {
	        
        $message['status'] = false;

        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $config = array(
                array('field' => 'uName', 'label' => 'Username', 'rules' => 'trim|required|xss_clean'),
                array('field' => 'uPass', 'label' => 'Password', 'rules' => 'trim|required|xss_clean')
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                $json = array(
                    'uName' => $this->input->post('uName'),
                    'uPass' => $this->input->post('uPass')
                );

                $params = json_encode($json);
                $response = json_decode($this->api_model->validate($params), true);
				
                if ($response['code'] == 4) {
					
					$session_data = array(
                        'agent_data' => $response['message'],
                        'is_logged_in_agent' => true
                    );
                    $this->session->set_userdata($session_data);
					
				    redirect();
                } else {

                    $message['status'] = true;
                    $message['type'] = 'error_message';
                    $message['message'] = 'Invalid Details. Please check Username and Password combination.';

                    $this->session->set_flashdata('message', $message);
                    redirect('auth');
                }
            } else {
                $message['status'] = true;
                $message['type'] = 'error_message';
                $message['message'] = validation_errors();

                $this->session->set_flashdata('message', $message);
                redirect('auth');
            }
        } else {
            $this->template->display('user/login.tpl');
        }
    }

    
	// Logout clear all session data
    public function logout() {
        $this->session->sess_destroy();
        redirect();
    }
}
