<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recommend extends CI_Controller {

	function __construct() {
        parent::__construct();
        is_logged_in();
    }
	
	public function index()
	{
		$recommend_form = array(
            'id' 			=> 'recommend_form',
            'name' 			=> 'recommend_form',
            'autocomplete' 	=> 'off',
            'method' 		=> 'post',
            'class' 		=> 'form-horizontal form-label-left',
            'novalidate' 	=> 'novalidate'
        );
        $this->template->assign('recommend_form', $recommend_form);
		
		$this->template->display('recommend.tpl');	
	}
}
