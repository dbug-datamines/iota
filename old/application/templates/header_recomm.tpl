<!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>{$smarty.const.SITE_TITLE}</title>
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/fonts.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/crumina-fonts.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/normalize.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/grid.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/styles.css">
<!--Plugins styles-->
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/primary-menu.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/magnific-popup.css">
<link href="{$smarty.const.ASSET_PATH}/css/hexa.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<link href="{$smarty.const.ASSET_PATH}/css/smart_wizard.css" rel="stylesheet" type="text/css" />
    
<!-- Optional SmartWizard theme -->
<link href="{$smarty.const.ASSET_PATH}/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />
	
<!--Styles for RTL-->
<!--<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/rtl.css">-->
<!--External fonts-->
<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/iota-style.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/style.css">
<link rel='stylesheet' href='{$smarty.const.ASSET_PATH}css/rangeslider.css'>
<link rel='stylesheet' href='{$smarty.const.ASSET_PATH}css/rangeslider_override.css'>

</head>
<body class=" ">
{nocache}
<!-- Header -->
<!--<header class="header" id="site-header">-->
  <div class="container">
    <div class="header-content-wrapper">
    
    
    
    
      <div class="logo"> <a href="{$smarty.const.BASE_URL}" class=""><img src="{$smarty.const.ASSET_PATH}images/logo.png" /></a> 
      
      
      
      <span class="contactus pull-right">
  <span class="glyphicon glyphicon-user  icon-size"></span>
 </span>
        <div class="show-contact">
      <ul class="nav navbar-nav pull-right company-info">
        <li><strong>D-bug Datamines Pvt. Ltd.</strong></li>
        <li><i class="fa fa-phone-square"></i>+91 9087275584</li>
        <li><i class="fa fa-envelope"></i>contact@d-bugdatalabs.com</li>
         </ul>
         </div>
         
            </div>
           
    </div>
  </div>
<!--</header>-->
{/nocache}

