<!-- JS Script -->
<div id="backtotop" style="display: block;"> <a href="#costumModal23" role="button" class="btn costumModal23 btn-default" data-toggle="modal">Help </a></div>
<div id="costumModal23" class="modal iota" data-easein="perspectiveUpIn" tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
  <div class="modal-dialog">
   
    <div class="modal-body">
     <div class="col-md-2 iotamessage-img  pd0">
      <img src="assets/images/iota_imgl.png"> 
      </div>
       <div class="col-md-10 iotamsg-div">
       <div id="iotamessage" class="left"></div></div>
      </div>
  </div>
</div>
<script>
var i = 0;
var txt = 'Let’s start with something about yourself! ';
var speed = 75;

function typeWriter() {
  if (i < txt.length) {
    document.getElementById("iotamessage").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }
}
window.onload = function() {
  typeWriter("window onload");
};

</script> 


<script src="{$smarty.const.ASSET_PATH}/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src='{$smarty.const.ASSET_PATH}js/rangeslider.js'></script> 
<script src="{$smarty.const.ASSET_PATH}/js/crum-mega-menu.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/swiper.jquery.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/theme-plugins.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/main.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/form-actions.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/velocity.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/ScrollMagic.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/animation.velocity.min.js"></script>
<!--<script src="{$smarty.const.ASSET_PATH}/js/validator/validator.js"></script>-->

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<script src="{$smarty.const.ASSET_PATH}/js/validator.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/jquery.smartWizard.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/jquery.smartWizard.min.js"></script>


{literal}
<script>
$(document).ready(function () {

$('.img-check').click(function(e) {
	$('.img-check').not(this).removeClass('check')
		.siblings('input').prop('checked',false);
	$(this).addClass('check')
		.siblings('input').prop('checked',true);
});

$('.img-check-prefernce').click(function(e) {
	$('.img-check-prefernce').not(this).removeClass('check')
		.siblings('input').prop('checked',false);
	$(this).addClass('check')
		.siblings('input').prop('checked',true);
});

$('.img-check-gender').click(function(e) {
	$('.img-check-gender').not(this).removeClass('check')
		.siblings('input').prop('checked',false);
	$(this).addClass('check')
		.siblings('input').prop('checked',true);
});

var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ 
                                                    if( !$(this).hasClass('disabled')){ 
                                                        var elmForm = $("#myForm");
                                                        if(elmForm){
                                                            elmForm.validator('validate'); 
                                                            var elmErr = elmForm.find('.has-error');
                                                            if(elmErr && elmErr.length > 0){
                                                                alert('Oops we still have error in the form');
                                                                return false;    
                                                            }else{
                                                                alert('Great! we are ready to submit form');
                                                                elmForm.submit();
                                                                return false;
                                                            }
                                                        }
                                                    }
                                                });
var btnCancel = $('<button></button>').text('Cancel')
								 .addClass('btn btn-danger')
								 .on('click', function(){ 
										$('#smartwizard').smartWizard("reset"); 
										$('#myForm').find("input, textarea").val(""); 
									});                         
            
// Smart Wizard
$('#smartwizard').smartWizard({ 
		selected: 0, 
		theme: 'dots',
		transitionEffect:'fade',
		toolbarSettings: {toolbarPosition: 'bottom',
						  toolbarExtraButtons: [btnFinish, btnCancel]
						},
		anchorSettings: {
					markDoneStep: true, // add done css
					markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
					removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
					enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
				}
	 });

$("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
	var elmForm = $("#form-step-" + stepNumber);
	// stepDirection === 'forward' :- this condition allows to do the form validation 
	// only on forward navigation, that makes easy navigation on backwards still do the validation when going next
	if(stepDirection === 'forward' && elmForm){
		elmForm.validator('validate'); 
		var elmErr = elmForm.children('.has-error');
		if(elmErr && elmErr.length > 0){
			// Form validation failed
			return false;    
		}
	}
	return true;
});

$("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
	// Enable finish button only on last step
	if(stepNumber == 3){ 
		$('.btn-finish').removeClass('disabled');  
	}else{
		$('.btn-finish').addClass('disabled');
	}
}); 

var val = $("#valR").val(); 
$("#range-test").html(val);
var srcPic = "assets/images/sweet/"+ val + ".png";
$("#img-test").attr("src", srcPic);
$('#valR').change(function(e) {
	var val = $("#valR").val(); 
    $("#range-test").html(val);
	var srcPic = "assets/images/sweet/"+ val + ".png";
  	$("#img-test").attr("src", srcPic);
});

var val = $("#valR1").val(); 
$("#range-test1").html(val);
var srcPic = "assets/images/spicy/"+ val + ".png";
$("#img-test1").attr("src", srcPic);	
$('#valR1').change(function(e) {
	var val = $("#valR1").val(); 
    $("#range-test1").html(val);
	var srcPic = "assets/images/spicy/"+ val + ".png";
  	$("#img-test1").attr("src", srcPic);
});
				
var val = $("#valR2").val(); 
$("#range-test2").html(val);
var srcPic = "assets/images/cheesy/"+ val + ".png";
$("#img-test2").attr("src", srcPic);	
$('#valR2').change(function(e) {
	var val = $("#valR2").val(); 
    $("#range-test2").html(val);
	var srcPic = "assets/images/cheesy/"+ val + ".png";
  	$("#img-test2").attr("src", srcPic);
});

var val = $("#valR3").val(); 
$("#range-test3").html(val);
var srcPic = "assets/images/gravy_soupy/"+ val + ".png";
$("#img-test3").attr("src", srcPic);	
$('#valR3').change(function(e) {
	var val = $("#valR3").val(); 
    $("#range-test3").html(val);
	var srcPic = "assets/images/gravy_soupy/"+ val + ".png";
  	$("#img-test3").attr("src", srcPic);
});

var val = $("#valR4").val(); 
$("#range-test4").html(val);
var srcPic = "assets/images/sour/"+ val + ".png";
$("#img-test4").attr("src", srcPic);	
$('#valR4').change(function(e) {
	var val = $("#valR4").val(); 
    $("#range-test4").html(val);
	var srcPic = "assets/images/sour/"+ val + ".png";
  	$("#img-test4").attr("src", srcPic);
});


	$('#myCarousel').carousel({
        interval: false,
		wrap: false
    })
   	
		$(".item1, .item2, .item3").click(function () {
			//alert(1);
            $( ".right.carousel-control" ).trigger( "click" );
        });
        $(".item4, .item5, .item6").click(function () {
            $( ".right.carousel-control" ).trigger( "click" );
        });
        $(".item7, .item8, .item9").click(function () {
            alert(2);
        });
					

});
</script>

<script>
  $( document ).ready(function() {
  // Handler for .ready() called.
    $('.slide').removeClass('active');
  });
  $('input[type="range"]').rangeslider({
      polyfill: false,
      rangeClass: 'rangeslider',
      fillClass: 'rangeslider__fill',
      handleClass: 'rangeslider__handle',
      onInit: function() {

      },
      onSlide: function(position, value) {
          $('.rangeslider__handle').attr('data-content', value);
          $('.done').removeClass('active');
          $('.slide').addClass('active');
      },
      onSlideEnd: function(position, value) {
        $('.done').addClass('active');
        $('.slide').removeClass('active');
      }
  });
  </script>
  <script>
   $(document).ready(function () {				
					 $('#costumModal23').modal('show');  
 						setTimeout(function() {					
					 $('#costumModal23').modal('hide');
					 }, 8000);
					 
				/*	 $('.costumModal23').click(function(e) {
					  $('#costumModal23').css('display','block');  
 						setTimeout(function() {					
					 $('#costumModal23').modal('hide');
					 }, 6000);
					 */
					 
			});	
			
				  $(".contactus").hover(function(){
        $(".show-contact").slideToggle("slow");
		
    });
  
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

			
			$(".hex-inner").mouseover(function(){
        $(".hex-inner .image-text").css("display", "none");
    });
    $(".hex-inner").mouseout(function(){
        $(".hex-inner .image-text").css("display", "block");
    });
	$(".hex-inner1").mouseover(function(){
        $(".hex-inner1 .image-text").css("display", "none");
    });
    $(".hex-inner1").mouseout(function(){
        $(".hex-inner1 .image-text").css("display", "block");
    });
	
	$(".hex-inner2").mouseover(function(){
        $(".hex-inner2 .image-text").css("display", "none");
    });
    $(".hex-inner2").mouseout(function(){
        $(".hex-inner2 .image-text").css("display", "block");
    });
	
	$(".hex-inner3").mouseover(function(){
        $(".hex-inner3 .image-text").css("display", "none");
    });
    $(".hex-inner3").mouseout(function(){
        $(".hex-inner3 .image-text").css("display", "block");
    });
	
	$(".hex-inner4").mouseover(function(){
        $(".hex-inner4 .image-text").css("display", "none");
    });
    $(".hex-inner4").mouseout(function(){
        $(".hex-inner4 .image-text").css("display", "block");
    });
	
	$(".hex-inner5").mouseover(function(){
        $(".hex-inner5 .image-text").css("display", "none");
    });
    $(".hex-inner5").mouseout(function(){
        $(".hex-inner5 .image-text").css("display", "block");
    });
   
	
		});
         </script>
         
{/literal}
<!-- ...end JS Script -->
</body></html>