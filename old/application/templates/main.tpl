{include file="header.tpl"}

<div class="content-wrapper">




<div class="container-fluid">
    <div class="row bg-border-color medium-padding100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <img src="{$smarty.const.ASSET_PATH}img/eat.png" alt="IOTA"  >
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="heading">
                        <h4 class="h1 heading-title">Make Smarter Decisions</h4>
                        <div class="heading-line">
                            <span class="short-line"></span>
                            <span class="long-line"></span>
                        </div>
						{include file="message.tpl"}
                        <p class="heading-text">An Artificial Intelligence powered recommendation engine
                        </p>
						{nocache}
						<a {if $is_login} href="{$smarty.const.BASE_URL}recommend" {else} href="#" {/if} class="btn btn-medium btn--secondary {if !$is_login}user-menu-content js-open-aside{/if}">
							<span class="text">Start</span>
							<span class="semicircle"></span>
						</a>
						{/nocache}
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>


</div>

{include file="footer.tpl"}