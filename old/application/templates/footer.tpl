<!-- Footer -->

<footer class="footer js-fixed-footer" id="site-footer">
  <div class="sub-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <span> {$smarty.const.COPY} </span> 
		
		  
		  </div>
      </div>
    </div>
  </div>
</footer>
<!-- JS Script -->
<script src="{$smarty.const.ASSET_PATH}/js/jquery-2.1.4.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/crum-mega-menu.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/swiper.jquery.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/theme-plugins.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/main.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/form-actions.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/velocity.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/ScrollMagic.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/animation.velocity.min.js"></script>
<script src="{$smarty.const.ASSET_PATH}/js/validator/validator.js"></script>

{literal}
<script>
$(document).ready(function () {

$("#agent_register_login_submit").click(function (e) {

		$("#error-login").hide();
		
        e.preventDefault();
        var submit = true;

        if (!validator.checkAll($("#agent_login_form"))) {
            submit = false;
        }

        if (submit) {
			//alert(1);
            $('#agent_login_form').submit();
        } else {
			//alert(2);
			$("#error-login").show();
			
            return false;
        }
    }); 

});
</script>
{/literal}
<!-- ...end JS Script -->
</body></html>