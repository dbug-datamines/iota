{strip}
{nocache}
      
{if $message.status==true}

	{if $message.type=='error_message'}
		<div class="alert alert-danger alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span> </button>
		<strong>Error!</strong> {$message.message} </div>
	{/if}
	
	{if $message.type=='success_message'}
		<div class="alert alert-success alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span> </button>
		<strong>Success!</strong> {$message.message} </div>
	{/if}
	
	{/if}
      
{/nocache}
{/strip}