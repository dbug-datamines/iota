<!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>{$smarty.const.SITE_TITLE}</title>
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/fonts.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/crumina-fonts.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/normalize.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/grid.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/styles.css">
<!--Plugins styles-->
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/primary-menu.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/bootstrap_modal.css">
<!--Styles for RTL-->
<!--<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/rtl.css">-->
<!--External fonts-->
<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120477304-1"></script>
{literal}
    <script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120477304-1');
</script>
{/literal} 

</head>
<body class=" ">
<!-- Header -->
<header class="header" id="site-header">
  <div class="container">
    <div class="header-content-wrapper">
      <div class="logo"> <a href="{$smarty.const.BASE_URL}" class="full-block-link"></a> <img src="{$smarty.const.ASSET_PATH}/images/logo_new.png">
        <div class="logo-text"> 
          <!-- <div class="logo-title">{$smarty.const.SITE_TITLE}</div>
          <div class="logo-sub-title">{$smarty.const.SUB_SITE_TITLE}</div> --> 
        </div>
      </div>
      <div class="pull-right translation" id="google_translate_element"></div>
      <div class="user-menu open-overlay"> <a href="#" class="user-menu-content  js-open-aside"> <span></span> <span></span> <span></span> </a> </div>
    </div>
  </div>
</header>
<!-- ... End Header --> 
<!-- Right-menu --> 

<!-- <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">

                   

                    <div class="modal-content">

                        {* <div class="modal-header">
                           
                        <h4 class="modal-title">Rate Your Experience</h4>
                        </div>*}
                        <div class="modal-body">
                            <span>This application requires access to your location. Please allow the same in your browser settings.</span>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
                        </div>
                    </div>


                </div>
            </div>  -->
<div class="mCustomScrollbar" data-mcs-theme="dark">
  <div class="popup right-menu">
    <div class="right-menu-wrap">
      <div class="user-menu-close js-close-aside"> <a href="#" class="user-menu-content  js-clode-aside"> <span></span> <span></span> </a> </div>
      <div class="logo"> 
        <!--<a href="index.html" class="full-block-link"></a> <img src="img/logo-eye.png" alt="Seosight">-->
        <div class="logo-text">
          <div class="logo-title"><img src="{$smarty.const.ASSET_PATH}/images/logo_new.png"></div>
        </div>
      </div>
      <p class="text">{$smarty.const.SUB_SITE_TITLE}</p>
    </div>
    {nocache}
    {if !$is_login}
    {form_open('auth/validate',$agent_login_form)}
    <div class="widget login">
      <h4 class="login-title">Sign In to Your Account</h4>
      <span id="error-login" style=" display:none; color: #f15b26;">
      <p>Please enter details.</p>
      </span>
      <input type="hidden" id="latitude-form" value="" name="latitude-form">
      <input type="hidden" id="longitude-form" value="" name="longitude-form">
      <input class="email input-standard-grey" placeholder="Username or Email" type="text" name="uName" required id="uName">
      <input class="password input-standard-grey" placeholder="Password" type="password"  name="uPass" required>
      <div class="login-btn-wrap">
        <div class="btn btn-medium btn--dark btn-hover-shadow" id="agent_register_login_submit" data-loading-text="Processing..."> <span class="text">login now</span> <span class="semicircle"></span> </div>
        <!--<div class="remember-wrap">
          <div class="checkbox">
            <input id="remember" type="checkbox" name="remember" value="remember">
            <label for="remember">Remember Me</label>
          </div>
        </div>--> 
      </div>
      <!--<div class="helped">Lost your password?</div>
      <div class="helped">Register Now</div>--> 
    </div>
    {form_close()}
    {/if}
    {/nocache}
    <div class="widget contacts">
      <h4 class="contacts-title">Get In Touch</h4>
      <!--<p class="contacts-text">Lorem ipsum dolor sit amet, duis metus ligula amet in purus,
        vitae donec vestibulum enim, tincidunt massa sit, convallis ipsum. </p>--> 
      <!--<div class="contacts-item"> <img src="{$smarty.const.ASSET_PATH}/img/contact4.png" alt="phone">
        <div class="content"> <a href="#" class="title">+91 9087275584</a>
          <p class="sub-title">Mon-Fri 9am-6pm</p>
        </div>
      </div> -->
      <div class="contacts-item"> <img src="{$smarty.const.ASSET_PATH}/img/contact5.png" alt="phone">
        <div class="content"> <a href="#" class="title">contact@d-bugdatamines.com</a> 
          <!--<p class="sub-title">online support</p>--> 
        </div>
      </div>
      <div class="contacts-item"> <img src="{$smarty.const.ASSET_PATH}/img/contact6.png" alt="phone">
        <div class="content"> <a href="#" class="title">D-bug Datamines Pvt. Ltd.</a> 
          <!--<p class="sub-title">Mumbai, India</p>--> 
        </div>
      </div>
      {nocache}
      {if $is_login} <a href="{$smarty.const.BASE_URL}auth/logout" class="btn btn-medium btn--secondary "> <span class="text">Logout</span> <span class="semicircle"></span> </a> {/if}
      {/nocache} 
      
      <!-- Modal --> 
      
    </div>
  </div>
</div>
<div class="modal fade" id="myModal_error" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p>Please Allow Your Location.</p>
        <p><strong>Note:</strong> This application requires access to your location. Please allow the same in your browser settings.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal_geo" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p>Not Allowed.</p>
        <p><strong>Note:</strong> You are not having permission to access to our application.Try it on our own zone.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ... End Right-menu --> 
