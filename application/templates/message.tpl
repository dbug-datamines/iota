{strip}
{nocache}
      
{if $message.status==true}

	{if $message.type=='error_message'}
	
	<a href="javascript:void(0);" class="btn btn-medium btn-border c-primary">
		<span class="text"><div class="alert alert-danger alert-dismissible fade in" role="alert">
	
	<strong>Error!</strong> {$message.message} </div></span>
		<span class="semicircle"></span>
	</a>
						
		
	{/if}
	
	{if $message.type=='success_message'}
		<a href="javascript:void(0);" class="btn btn-medium btn-border c-primary">
			<span class="text"><div class="alert alert-success alert-dismissible fade in" role="alert">
		
		<strong>Success!</strong> {$message.message} </div></span>
			<span class="semicircle"></span>
		</a>
		
	{/if}
	
	{/if}
      
{/nocache}
{/strip}