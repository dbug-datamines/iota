<!-- Footer -->

<footer class="footer js-fixed-footer" id="site-footer">
  <div class="sub-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"> <span> {$smarty.const.COPY} </span> </div>
      </div>
    </div>
  </div>
</footer>

<!-- JS Script --> 
<script src="{$smarty.const.ASSET_PATH}js/jquery-2.1.4.min.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/crum-mega-menu.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/swiper.jquery.min.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/theme-plugins.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/main.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/form-actions.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/velocity.min.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/ScrollMagic.min.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/animation.velocity.min.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/bootstrap_modal.js"></script> 
<script src="{$smarty.const.ASSET_PATH}js/validator/validator.js"></script> 
{literal} 
<script>

        filePath = '{/literal}{$smarty.const.BASE_URL}{literal}';


        function Login(msg) {

            if (msg == "yes")
            {
               
                $("#error-login").hide();

                var proceed = $("#agent_register_login_submit").html();
                var processing = $("#agent_register_login_submit").attr("data-loading-text");

                $("#agent_register_login_submit").attr("disabled", true);
                $("#agent_register_login_submit").text(processing);

                // e.preventDefault();
                var submit = true;

                if (!validator.checkAll($("#agent_login_form"))) {
                    submit = false;
                }

                if (submit) {

                    $('#agent_login_form').submit();

                } else {

                    $("#error-login").show();
                    return false;
                }
            } else {
                
                $('#myModal_geo').modal('show');
                //alert("You are Not having permission to login");
            }

        }

//someFunction();

        function someFunction() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(showLocation, showError);

            } else {
                $('#location').html('Geolocation is not supported by this browser.');
            }

        }

        function showError(error) {

               $('#myModal_error').modal('show');
            //alert("Please update setting part");

            return true;
        }

        function showLocation(position) {

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            
             $('#latitude-form').val(latitude);
              $('#longitude-form').val(longitude);

                $("#error-login").hide();

                var proceed = $("#agent_register_login_submit").html();
                var processing = $("#agent_register_login_submit").attr("data-loading-text");

                $("#agent_register_login_submit").attr("disabled", true);
                $("#agent_register_login_submit").text(processing);

                // e.preventDefault();
                var submit = true;

                if (!validator.checkAll($("#agent_login_form"))) {
                    submit = false;
                }

                if (submit) {

                    $('#agent_login_form').submit();

                } else {

                    $("#error-login").show();
                    return false;
                }


        }

    </script> 
<script>

        $(document).ready(function () {

            var i = 0;
            var txt = 'Hi! I\'m IOTA. Welcome to ask IOTA. Let me help you choose your meal ';
            var speed = 75;

            function typeWriter() {
                if (i < txt.length) {
                    document.getElementById("demo").innerHTML += txt.charAt(i);
                    i++;
                    setTimeout(typeWriter, speed);
                }
            }
            window.onload = function () {
                typeWriter("window onload");
            };


           $("#agent_register_login_submit").click(function (e) {
               
                  var proceed = $("#agent_register_login_submit").html();
                var processing = $("#agent_register_login_submit").attr("data-loading-text");

                $("#agent_register_login_submit").attr("disabled", true);
                $("#agent_register_login_submit").text(processing);
  someFunction();
    }); 

        });
    </script> 
<script>
$(document).ready(function(){
    
    $("#myBtn2").click(function(){
        $("#myModal2").modal({backdrop: false});
    });
   
});
</script> 
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es,fr,ja', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> 
{/literal} 
<!-- ...end JS Script -->
</body></html>