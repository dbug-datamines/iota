{include file="header.tpl"}
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row bg-border-color medium-padding100 pd-bt15 mg-top-home">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center"> <img src="{$smarty.const.ASSET_PATH}img/eat.png" alt="IOTA"  ><span class="success_msg"> {include file="message.tpl"} </span></div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="heading">
              <h4 class="h1 heading-title">Make Smarter Decisions</h4>
              <div class="heading-line"> <span class="short-line"></span> <span class="long-line"></span> </div>
              {nocache}
              <div class="testimonial-item"> 
                <!-- Slider main container -->
                <div class="swiper-container testimonial__thumb overflow-visible swiper-swiper-unique-id-1 initialized swiper-container-horizontal swiper-container-fade" data-effect="fade" data-loop="false" id="swiper-unique-id-1">
                  <div class="swiper-wrapper" style="transition-duration: 0ms;">
                    <div class="testimonial-slider-item swiper-slide swiper-slide-prev" style="opacity: 0; transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                      <div class="testimonial-content">
                        <p class="heading-text"> An Artificial Intelligence powered recommendation engine </p>
                        <p class="text" data-swiper-parallax="-200" style="transform: translate3d(-200px, 0px, 0px); transition-duration: 0ms;">
                        <div id="demo" class="left connent-firstscreen"></div>
                        </p>
                        <p class="align-center" style="z-index:99999;"> <a {if $is_login} href="{$smarty.const.BASE_URL}recommend" {else} href="#" {/if} class="btn btn-medium btn--secondary {if !$is_login}user-menu-content js-open-aside{/if}"> <span class="text">Start</span> <span class="semicircle"></span> </a> </p>
                      </div>
                      <div class="avatar" data-swiper-parallax="-50" style="transform: translate3d(-50px, 0px, 0px); transition-duration: 0ms;"> <img class="iota-img" src="{$smarty.const.ASSET_PATH}/images/iota_imgl.png" alt="eat img"> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/nocache} </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade welcome-popup" id="myModal_1" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p> Welcome to <img src="{$smarty.const.ASSET_PATH}/images/logo_new.png"></p>
        <p> Restaurant Name {$restaurant_name}</p>
      </div>
    </div>
  </div>
</div>
{include file="footer.tpl"}
{literal} 
<script type="text/javascript">
 var filePath = '{/literal}{$smarty.const.BASE_URL}{literal}';
 $(window).load(function(){ 
     
   $('#myModal_1').modal('show');
 setTimeout(function() {
     $('#myModal_1').modal('hide');
 }, 3000000000000); 
   
   
    });
 $('#myModal_1').modal({
backdrop: 'static',
 keyboard: false  // to prevent closing with Esc button (if you want this too)
})

 window.setTimeout(function() {
  window.location.href=filePath+"recommend";
}, 5000);

 
 </script> 
{/literal}