<!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>{$smarty.const.SITE_TITLE}</title>
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/fonts.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/crumina-fonts.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/normalize.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/grid.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/styles.css">
<!--Plugins styles-->
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/primary-menu.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/magnific-popup.css">
<link href="{$smarty.const.ASSET_PATH}css/hexa.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{$smarty.const.ASSET_PATH}css/bootstrap.min.css">
<link href="{$smarty.const.ASSET_PATH}css/smart_wizard.css" rel="stylesheet" type="text/css" />

<!-- Optional SmartWizard theme -->
<link href="{$smarty.const.ASSET_PATH}css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />

<!--Styles for RTL-->
<!--<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}/css/rtl.css">-->
<!--External fonts-->
<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/iota-style.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.ASSET_PATH}css/style.css">
<link rel='stylesheet' href='{$smarty.const.ASSET_PATH}css/rangeslider.css'>
<link rel='stylesheet' href='{$smarty.const.ASSET_PATH}css/rangeslider_override.css'>
<link rel="stylesheet" type="text/css" href='{$smarty.const.ASSET_PATH}css/starratting.css'>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120477304-1"></script>
{literal}
    <script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120477304-1');
</script>
{/literal} 

</head>
<body class=" ">
{nocache} 
<!-- Header -->
<header class="header" >
  <div class="container">
    <div class="header-content-wrapper">
      <div class="logo"> <a href="{$smarty.const.BASE_URL}" class="full-block-link"></a> <img src="{$smarty.const.ASSET_PATH}/images/logo_new.png">
        <div class="logo-text"> 
          <!--  <div class="logo-title">{$smarty.const.SITE_TITLE}</div>
                                <div class="logo-sub-title">{$smarty.const.SUB_SITE_TITLE}</div>--> 
        </div>
      </div>
      <div class="pull-right translation" id="google_translate_element"></div>
    </div>
  </div>
</header>
<div class="modal rating-mobel-box fade" id="myModal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> {form_open('auth/add_ratting',$agent_login_form)}
      <div class="modal-header">
        <h4 class="modal-title">Rate Your Experience</h4>
      </div>
      <div class="modal-body"> <span class="my-rating-9"></span>
        <input type='hidden' name='rating' value="3.5" id='live-rating'>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-lg raised">SUBMIT</button>
        {form_close()}
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">LATER</button>
      </div>
    </div>
  </div>
</div>
<div class="modal rating-mobel-box fade" id="myModal_geo" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> {*
      <div class="modal-header">
        <h4 class="modal-title">Rate Your Experience</h4>
      </div>
      *}
      <div class="modal-body"> <span>You are Not having permission to login</span> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<div class="modal rating-mobel-box fade" id="myModal_error" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> {*
      <div class="modal-header">
        <h4 class="modal-title">Rate Your Experience</h4>
      </div>
      *}
      <div class="modal-body"> <span>Please update setting part</span> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<div class="modal myModal_iota_popup fade" id="myModal_iota_popup" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content"> 
      <div class="modal-header">
        <h4 class="modal-title">Rate Your Experience</h4>
      </div>
     
      <div class="modal-body"> <img src="assets/images/iota_imgl.png"> <div id="" class="left"> <h6>Hallo, Wij zijn Occhio!</h6></div> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-lg raised" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
{/nocache} 
