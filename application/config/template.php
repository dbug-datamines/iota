<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['debugging'] = false;
$config['debug_tpl'] = 'debug.tpl';
$config['caching'] = 1;
$config['force_compile'] = false;
