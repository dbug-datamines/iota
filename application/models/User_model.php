<?php

/*
  |--------------------------------------------------------------------------
  | User Model class
  | @author	Pentation Team
  |--------------------------------------------------------------------------
 */

class User_model extends CI_Model {

    // password encription
    public function __encrip_password($password) {
        return $this->__bcrypt_password($password);
    }

    // BCRYPT password encription
    public function __bcrypt_password($password) {
        return $this->bcrypt->hash_password($password);
    }

    // Genetating randon password
    public function random_password($length = 8) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $password = substr(str_shuffle($chars), 0, $length);

        return $password;
    }

    //generate key for reset 
    public function generate_access_key() {
        return md5(microtime() . rand());
    }

    // Login Authentication of user
    public function validate($username, $password, $params) {
        
        $latitude = $params['latitude'];
        $longitude = $params['longitude']; 
        $additional_params = check_user_agent();
        $device_info = serialize($additional_params);
        

        $this->load->helper('email');

        // Valid Email Check
        if (valid_email($username)) {
            $email = $username;
            $this->db->where('email', $email);

            // Valid Mobile Check, 10 Digit Only
        } elseif (strlen($username) <= 15) {
           
            //$mobile = $username;
            $this->db->where('username', $username);

            // None of the above then return false
        } else {
            return false;
        }

        $this->db->where('status', 'enable');
        $this->db->limit(1);
        $query = $this->db->get('user_agent');

        $user = array();
        $valid = false;

        if ($query->row()) {
           

            $row = $query->row();
            
            
   

            // Password Check with BCrypt Encryption Method
            if ($this->bcrypt->check_password($password, $row->password)) {
                $valid = true;
            } else {                         
                return false;
            }
        }
        
       
        if ($valid) {
            
            
            $latitude = $params['latitude'];
            $longitude = $params['longitude'];

            $user = $this->get_location($row->user_agent_id);

            $const_latitude = $user['latitude'];
            $const_longitude = $user['longitude'];

            $distance = $this->getDistance($latitude, $longitude, $const_latitude, $const_longitude);
            if ($distance > 0.5) {
                $user = $this->get_user_details($row->user_agent_id);
                $user['permission'] = '1';
                $remarks="Location Allowed to User";
            } else {

                $user = $this->get_user_details($row->user_agent_id);
                $user['permission'] = '0';
                $remarks="Location Not Allowed to User";
            }




            //insert into login_logs

            $_set_data = array(
                'user_agent_id' => $user['user_agent_id'],
                'IP' => $this->input->ip_address(),
                'date_time' => date('Y-m-d H:i:s', time()),
                'device_info' =>$device_info,
                'username' =>$user['username'],
                'latitude' => $latitude,
                'longitude' => $longitude,
                'login_status' => 'success',
                'remarks' => $remarks,
            );

            $this->db->insert('login_logs', $_set_data);


            return $user;
        } else {
              $remarks="Invalid Username Or Password";
            
               $_set_data = array(
                'user_agent_id' => '0',
                'IP' => $this->input->ip_address(),
                'date_time' => date('Y-m-d H:i:s', time()),
                'device_info' =>$device_info,
                'username' =>$username,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'login_status' => 'fail',
                'remarks' => $remarks,
            );

            $this->db->insert('login_logs', $_set_data);
          
            return false;
        }
    }

    public function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {
        $earth_radius = 6371;

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return $d;
    }

    //get user details
    public function get_user_details($user_id) {

        $this->db->select('ua.*,ug.user_group_name');
        $this->db->from('user_agent AS ua');
        $this->db->join('user_group AS ug', 'ua.user_group_id=ug.user_group_id');
        $this->db->where('ua.user_agent_id', $user_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $row = $query->row_array();

            if (empty($row['profile_image'])) {
                $row['profile_image'] = 'avatar5.png';
            }

            $row['display_status'] = $this->display_user_status($row['status']);
            $row['display_date_added'] = date_display($row['date_added']);

            return $row;
        } else {
            return false;
        }
    }

    //Display user status with CSS span
    public function display_user_status($status) {
        switch ($status) {
            case 'disable':
                $user_status = '<span class="label label-warning">' . ucfirst($status) . '</span>';
                break;
            case 'enable':
                $user_status = '<span class="label label-success">' . ucfirst($status) . '</span>';
                break;
        }

        return $user_status;
    }

    // At the time of registration 
    // Check user already exist
    public function check_user_exist($email, $mobile) {
        $this->db->where('email', $email);
        $this->db->or_where('mobile', $mobile);

        $query = $this->db->get('user_agent');

        //echo $this->db->last_query(); die;

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //From mobile check user exist
    public function check_user_exist_mobile($mobile) {
        $this->db->select('verified,verified_email,mobile,status');
        $this->db->where('mobile', $mobile);
        $query = $this->db->get('user_agent');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    // From email check user exist
    public function check_user_exist_email($email) {
        $this->db->where('email', $email);
        $query = $this->db->get('user_agent');

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //Change user login password
    public function change_password($data) {

        $_set_data = array(
            'password' => $this->__encrip_password($data['password'])
        );

        $this->db->where('user_agent_id', $data['user_agent_id']);
        $this->db->update('user_agent', $_set_data);

        if ($this->db->affected_rows() == 1) {
            return $this->get_user_details($data['user_agent_id']);
        } else {
            return false;
        }
    }

    public function get_user_group_list() {

        //$this->db->select('*');
        //$this->db->from('user_group');
        $query = $this->db->get('user_group');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Add User
    public function add_new_user($data) {

        //$password = shortId('6', $type = 'number');
        $password = "123abc123";
        $enc_password = $this->__encrip_password($password);

        $set_data = array(
            'exp_date' => date('Y-m-d', strtotime($data['exp_date'])),
            'first_name' => $data['first_name'],
            'profile_image' => 'avatar5.png',
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'password' => $enc_password,
            'user_group_id' => $data['user_group_id'],
            'status' => $data['status'],
            //'user_added_by' => $data['admin_data']['user_bcc_id'],
            'date_added' => date('Y-m-d H:i:s', time())
        );

        if ($this->db->insert('user_agent', $set_data)) {

            if ($_FILES) {

                $this->load->library('image_lib');

                $file_upload_path = 'assets/images/user_profile';

                if (!is_dir($file_upload_path))
                    mkdir($file_upload_path, 0777, TRUE);

                $config ['upload_path'] = $file_upload_path;
                $config ['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|PNG|JPG';
                $config ['max_size'] = '1000';
                $config ['overwrite'] = FALSE;
                $config ['file_name'] = $user_emp_id . '_' . md5(time());

                $this->load->library('upload', $config);

                $this->upload->initialize($config);

                if ($_FILES['profile_photo']) {

                    if ($_FILES['profile_photo']['error'] == 0) {
                        if (!$this->upload->do_upload('profile_photo')) {
                            $error = $this->upload->display_errors();
                        } else {
                            $file_data = $this->upload->data();
                            $profile_photo = $file_data['file_name'];

                            // process resize image before upload
                            $configer = array(
                                'image_library' => 'gd2',
                                'source_image' => $file_data['full_path'],
                                'create_thumb' => FALSE, //tell the CI do not create thumbnail on image
                                'maintain_ratio' => TRUE,
                                'quality' => '80%', //tell CI to reduce the image quality and affect the image size
                                'width' => 250, //new size of image
                                'height' => 250, //new size of image
                            );
                            $this->image_lib->clear();
                            $this->image_lib->initialize($configer);
                            $this->image_lib->resize();

                            $_set_data = array(
                                'user_profile' => $profile_photo
                            );

                            $this->db->where('user_bcc_id', $user_emp_id);
                            $this->db->update('user_bcc', $_set_data);
                        }
                    }
                    unset($_FILES['profile_photo']);
                }
            }

            return true;
        } else {
            return false;
        }
    }

    //add ratting
    public function add_ratting($data) {



        $set_data = array(
            'user_agent_id' => $data['customer_id'],
            'rating' => $data['rating'],
            'date_added' => date('Y-m-d H:i:s', time())
        );

        if ($this->db->insert('user_rating', $set_data)) {

            return $set_data;
        } else {

            return false;
        }
    }

    public function get_location($id) {
        $this->db->select('latitude,longitude');
        $this->db->where('user_agent_id', $id);
        $query = $this->db->get('user_agent');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

}
