<?php

/*
  |--------------------------------------------------------------------------
  | Response Codes
  | 1. Params could not be found
  | 2. Misssing Parameter
  | 3. Invalid Details. Please check Email/Mobile and Password combination. Login service
  | 4. successful login, return user data
  | 23. password updated | returns user_data
  | 24. Error in updating password. Contact admin.
  |------------------------------------------------------------------------------------------
 */

class Api_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        //$this->load->model('product_model');
        //$this->load->model('encryption_model');
    }

    //json decoding the request data
    public function request($data) {
        return json_decode($data, true);
    }

    //json encoding the response data
    public function response($data = NULL) {
        return json_encode($data, true);
    }

    //login validation function
    public function validate($params) {
        $params = $this->request($params);

        if ($params) {

            $username = $params['uName'];
            $password = $params['uPass'];

            $is_valid = $this->user_model->validate($username, $password, $params);

            if ($is_valid) {

                return $this->response(array(
                            'code' => '4',
                            'status' => TRUE,
                            'message' => $is_valid
                ));
            } else {

                return $this->response(array(
                            'code' => '3',
                            'status' => FALSE,
                            'message' => 'Invalid Details. Please check Email/Mobile and Password combination.'
                ));
            }
        } else {

            return $this->response(array(
                        'code' => '1',
                        'status' => FALSE,
                        'message' => 'Params could not be found.'
            ));
        }
    }

    //change password
    public function change_password($params) {
        $params = $this->request($params);

        if ($params) {

            $user_data = $this->user_model->change_password($params);

            if ($user_data) {

                return $this->response(array(
                            'code' => '23',
                            'status' => TRUE,
                            'message' => $user_data
                ));
            } else {

                return $this->response(array(
                            'code' => '24',
                            'status' => FALSE,
                            'message' => 'Error in updating password. Contact admin.'
                ));
            }
        } else {

            return $this->response(array(
                        'code' => '1',
                        'status' => FALSE,
                        'message' => 'Params could not be found.'
            ));
        }
    }

    //Checking user details are already exist while registering
    public function check_user_exist($params) {
        $params = $this->request($params);

        if ($params) {

            $email = $params['email'];
            $mobile = $params['mobile'];

            if ($this->user_model->check_user_exist($email, $mobile)) {

                return $this->response(array(
                            'code' => '5',
                            'status' => TRUE,
                            'message' => 'User Already Exist.'
                ));
            } else {

                return $this->response(array(
                            'code' => '6',
                            'status' => FALSE,
                            'message' => 'User not Exist.'
                ));
            }
        } else {

            return $this->response(array(
                        'code' => '1',
                        'status' => FALSE,
                        'message' => 'Params could not be found.'
            ));
        }
    }

    // checking user mobile is already exist in DB
    public function check_user_exist_mobile($params) {
        $params = $this->request($params);

        if ($params) {

            $mobile = $params['mobile'];

            if ($this->user_model->check_user_exist_mobile($mobile)) {

                return $this->response(array(
                            'code' => '9',
                            'status' => TRUE,
                            'message' => 'Mobile Number Already Exist.'
                ));
            } else {

                return $this->response(array(
                            'code' => '10',
                            'status' => FALSE,
                            'message' => 'User mobile not Exist.'
                ));
            }
        } else {

            return $this->response(array(
                        'code' => '1',
                        'status' => FALSE,
                        'message' => 'Params could not be found.'
            ));
        }
    }

    // Get the user details from user ID
    public function get_user_details($params) {
        $params = $this->request($params);

        if ($params) {

            $user_agent_id = $params['user_agent_id'];

            $user_data = $this->user_model->get_user_details($user_agent_id);

            if ($user_data) {

                return $this->response(array(
                            'code' => '9',
                            'status' => TRUE,
                            'message' => $user_data
                ));
            } else {

                return $this->response(array(
                            'code' => '6',
                            'status' => FALSE,
                            'message' => 'User not Exist.'
                ));
            }
        } else {

            return $this->response(array(
                        'code' => '1',
                        'status' => FALSE,
                        'message' => 'Params could not be found.'
            ));
        }
    }

    //store rating
    public function add_ratting($params) {

        $params = $this->request($params);
        if ($params) {

            $user_data = $this->user_model->add_ratting($params);

            if ($user_data) {

                return $this->response(array(
                            'code' => '23',
                            'status' => TRUE,
                            'message' => $user_data
                ));
            } else {

                return $this->response(array(
                            'code' => '24',
                            'status' => FALSE,
                            'message' => 'Error in updating rating. Contact admin.'
                ));
            }
        } else {

            return $this->response(array(
                        'code' => '1',
                        'status' => FALSE,
                        'message' => 'Params could not be found.'
            ));
        }
    }
    
     //store rating
    public function get_location($params) {

        $params = $this->request($params);
        if ($params) {

            $user_data = $this->user_model->get_location($params);

            if ($user_data) {

                return $this->response(array(
                            'code' => '23',
                            'status' => TRUE,
                            'message' => $user_data
                ));
            } else {

                return $this->response(array(
                            'code' => '24',
                            'status' => FALSE,
                            'message' => 'Error in getting data. Contact admin.'
                ));
            }
        } else {

            return $this->response(array(
                        'code' => '1',
                        'status' => FALSE,
                        'message' => 'Params could not be found.'
            ));
        }
    }

}
