<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function index() {
		
		$message['status'] = false;

        if ($this->session->flashdata('message')) {
            $message = $this->session->flashdata('message');
        }
        $this->template->assign('message', $message);

		//If user is not login show him the login page
        if (!$this->session->userdata('is_logged_in_agent')) {
            redirect();
        } 
    }
	
	//Login verification
    public function validate() {
      
        
        $message['status'] = false;

        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $config = array(
                array('field' => 'uName', 'label' => 'Username', 'rules' => 'trim|required|xss_clean'),
                array('field' => 'uPass', 'label' => 'Password', 'rules' => 'trim|required|xss_clean')
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                $json = array(
                     
                    'uName' => $this->input->post('uName'),
                    'uPass' => $this->input->post('uPass'),
                    'latitude' => $this->input->post('latitude-form'),
                    'longitude' => $this->input->post('longitude-form'),
                );

                $params = json_encode($json);
                $response = json_decode($this->api_model->validate($params), true);
                
                		
                if ($response['code'] == 4) {
		   if ($response['message']['permission'] == 1) {			
					$session_data = array(
                        'agent_data' => $response['message'],
                        'is_logged_in_agent' => true
                    );
                    $this->session->set_userdata($session_data);
					
					$message['status'] = true;
                    $message['type'] = 'success_message';
                    $message['message'] = 'Welcome to IOTA.';

                    $this->session->set_flashdata('message', $message);
                    $this->template->assign('restaurant_name',$response['message']['restaurant_name']);
		     $this->template->display('main1.tpl');
                            }
                  else{
                      $message['status'] = true;
                    $message['type'] = 'success_message';
                    $message['message'] = 'You are not having permission to access to our application.Try it on our own zone.';

                    $this->session->set_flashdata('message', $message);
                    redirect();
                  }
				    
                } else {

                    $message['status'] = true;
                    $message['type'] = 'error_message';
                    $message['message'] = 'Invalid Details. Please check Username and Password combination.';

                    $this->session->set_flashdata('message', $message);
                    redirect();
                }
                
                 
            } else {
                $message['status'] = true;
                $message['type'] = 'error_message';
                $message['message'] = validation_errors();

                $this->session->set_flashdata('message', $message);
                redirect();
            }
        } else {
            $this->template->display('user/login.tpl');
        }
    }

    
	// Logout clear all session data
    public function logout() {
        $this->session->sess_destroy();
        redirect();
    }
    
        public function add_ratting() {
            $message['status'] = false;
            
             if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $config = array(
                array('field' => 'rating', 'label' => 'Ratting', 'rules' => 'trim|xss_clean'),
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {
                
                 $customer_data = $this->session->userdata('agent_data');

                $json = array(
                    
                    'rating' => $this->input->post('rating'),
                    'customer_id' =>$customer_data['user_agent_id']
                  
                );
                $json = json_encode($json);
                $response = json_decode($this->api_model->add_ratting($json), true);
                	
                if ($response['code'] == '23') {
                    
                      $message['status'] = true;
                    $message['type'] = 'success_message';
                    $message['message'] = 'Thank You For Your Rating';
			  $this->session->set_flashdata('message', $message);
                           //$this->template->display('main1.tpl');
		     redirect();
                } else {

                    $message['status'] = true;
                    $message['type'] = 'error_message';
                    $message['message'] = 'Invalid Input.';

                    $this->session->set_flashdata('message', $message);
                    redirect();
                }
            } else {
                $message['status'] = true;
                $message['type'] = 'error_message';
                $message['message'] = validation_errors();

                $this->session->set_flashdata('message', $message);
                redirect();
            }
        } else {
            $this->template->display('user/login.tpl');
        }
            
         
           
         
          
      }
      
      
    public function get_geolocation() {

        //echo '<pre>'; print_r($_REQUEST); die;
        $data = $_REQUEST;
        $latitude = $data['latitude'];
        $longitude = $data['longitude'];
        $geo_location = unserialize(GEO_LOCATION);
        
    
        
        $const_latitude = $geo_location['latitude'];
        $const_longitude = $geo_location['longitude'];
       
        $distance = $this->getDistance($latitude,$longitude,$const_latitude,$const_longitude);
        if ($distance < 1) {
            echo "yes";
        } else {
            echo "no";
        }
    }

    public function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {
        $earth_radius = 6371;

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return $d;
    }

      
}
