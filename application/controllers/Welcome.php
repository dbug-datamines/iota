<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
        parent::__construct();
        //is_logged_in();
    }
	
	public function index()
	{
		$message['status'] = false;

        if ($this->session->flashdata('message')) {
            $message = $this->session->flashdata('message');
        }
        $this->template->assign('message', $message);
		
		if (!$this->session->userdata('is_logged_in_agent')) {
            $is_login = false;
        } else {
            $is_login = true;
        }
		
		$this->template->assign('is_login', $is_login);
		
		$agent_login_form = array(
            'id' 			=> 'agent_login_form',
            'name' 			=> 'agent_login_form',
            'autocomplete' 	=> 'off',
            'method' 		=> 'post',
            'class' 		=> 'form-horizontal form-label-left',
            'novalidate' 	=> 'novalidate'
        );
        $this->template->assign('agent_login_form', $agent_login_form);
		
		$this->template->display('main.tpl');	
	}
}
