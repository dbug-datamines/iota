<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recommend extends CI_Controller {

	function __construct() {
        parent::__construct();
        is_logged_in();
    }
	
	public function index()
	{
		$recommend_form = array(
            'id' 			=> 'recommend_form',
            'name' 			=> 'recommend_form',
            'autocomplete' 	=> 'off',
            'method' 		=> 'post',
            'class' 		=> 'form-horizontal form-label-left',
            'novalidate' 	=> 'novalidate'
        );
        $this->template->assign('recommend_form', $recommend_form);
		
		$this->template->display('recommend.tpl');	
	}
	
	public function r_call(){
	
		//echo '<pre>'; print_r($_REQUEST); die;
		$data = $_REQUEST;
           
              
		//echo $data['chk_ing']['ing']['Garlic']; die;
		
		$file_write = 'data/input.csv';
		
		if (file_exists($file_write)) {
				
			unlink($file_write);
		}
		
		$insert = array();
		$insert[0] = 'ETHNICITY,PREFERENCE,GENDER,AGE,SWEET,SPICY,CHEESY,GRAVY_SOUP,SOUR,BASE,Garlic,Ginger,Coconut,Black pepper,Mustard,Aamchoor,Onion,Tomato,Soy,Tamarind';
		$out .= implode(",",$insert) . "\n";			
		
		$insert[0] = $data['chk1'].','.
					 $data['chk2'].','.
					 $data['chk3'].','.
					 $data['age'].','.
					 $data['taste_sweet'].','.
					 $data['taste_spicy'].','.
					 $data['taste_cheesy'].','.
					 $data['taste_gravy_soupy'].','.
					 $data['taste_sour'].','.
					 $data['chk_base'].','.
					 $data['chk_ing']['ing']['Garlic'].','.
					 $data['chk_ing']['ing']['Ginger'].','.
					 $data['chk_ing']['ing']['Coconut'].','.
					 $data['chk_ing']['ing']['Black pepper'].','.
					 $data['chk_ing']['ing']['Mustard'].','.
					 $data['chk_ing']['ing']['Aamchoor'].','.
					 $data['chk_ing']['ing']['Onion'].','.
					 $data['chk_ing']['ing']['Tomato'].','.
					 $data['chk_ing']['ing']['Soy'].','.					 
					 $data['chk_ing']['ing']['Tamarind'];
					 
		$out .= implode(",",$insert) . "\n";			
					
		if(file_put_contents($file_write, $out)){
		
			//$exe_file = ASSET_R_BASE_PATH."IOTA.R";
			//$e = exec(R_RATH.' '.$exe_file);
			
			//sleep(3);
			
			$file_output = 'data/output.csv';
			
			if (file_exists($file_output)) {
				
				$this->load->library('CSVReader');
        		
				$return = $this->csvreader->parse_file($file_output);
				
				
				
				echo json_encode($return);
				exit;
				//echo '<pre>'; print_r($return); die;
			}
	
		}
		
	
	}
}
