﻿<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Authentication class
  | @author	JR Team
  | @link	https://www.jaldirecharge.com
  | JaldiRecharge common functions Helpers
  |--------------------------------------------------------------------------
 */

// Get the user agent 
// - WEB
// - ROBOT
// - MOBILE 
function check_user_agent() {
    $CI = & get_instance();
    $CI->load->library('user_agent');

    if ($CI->agent->is_mobile()) {
        $agent['source'] = 'wap';
        $agent['source_info'] = $CI->agent->mobile() . '-' . $CI->agent->platform();
    } elseif ($CI->agent->is_browser()) {
        $agent['source'] = 'web';
        $agent['source_info'] = $CI->agent->browser() . '-' . $CI->agent->version() . '-' . $CI->agent->platform();
    } else {
        $agent['source'] = 'unidentified';
        $agent['source_info'] = $CI->agent->platform();
    }

    return $agent;
}

function is_logged_in() {
    $CI = & get_instance();
    $CI->load->library('session');

    if (!$CI->session->userdata('is_logged_in_agent')) {
        redirect('auth', 'refresh');
    } 
}

function date_display($date) {
    return date("F j, Y, g:i a", strtotime($date));
}

function makeComma($input) {
    if (strlen($input) <= 2) {
        return $input;
    }
    $length = substr($input, 0, strlen($input) - 2);
    $formatted_input = makeComma($length) . "," . substr($input, -2);
    return $formatted_input;
}

function money_display($num) {
    $pos = strpos((string) $num, ".");
    if ($pos === false) {
        /*
         *  $decimalpart = "00";
         */
        $decimalpart = "";
    }
    if (!($pos === false)) {
        $decimalpart = substr($num, $pos + 1, 2);
        $num = substr($num, 0, $pos);
    }

    if (strlen($num) > 3 & strlen($num) <= 12) {
        $last3digits = substr($num, -3);
        $numexceptlastdigits = substr($num, 0, -3);
        $formatted = makeComma($numexceptlastdigits);
        /*
         * $stringtoreturn = $formatted . "," . $last3digits . "." . $decimalpart;
         */
        $stringtoreturn = $formatted . "," . $last3digits;
    } elseif (strlen($num) <= 3) {
        /*
         * $stringtoreturn = $num . "." . $decimalpart;
         */
        $stringtoreturn = $num;
    } elseif (strlen($num) > 12) {
        $stringtoreturn = number_format($num, 2);
    }

    if (substr($stringtoreturn, 0, 2) == "-,") {
        $stringtoreturn = "-" . substr($stringtoreturn, 2);
    }

    return $stringtoreturn;
}

if (!function_exists('shortId')) {

    function shortId($length = 8, $type = 'mixed') {
        $key = '';
        if ($type == 'number') {
            $keys = array_merge(range(0, 9));
        } elseif ($type == 'small') {
            $keys = array_merge(range('a', 'z'));
        } elseif ($type == 'capital') {
            $keys = array_merge(range('A', 'Z'));
        } else {
            $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        }
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

}

if (!function_exists('mailJRC')) {

    function mailJRC($view_name = "", $to = "", $cc = '', $bcc = '', $subject = "", $sendername = 'Pentation', $email_data = array(), $file = '', $fromemail = '', $from = 'web') {

        $CI = & get_instance();

            //$CI = & get_instance();

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'smtp.pentationanalytics.com',
                'smtp_port' => 25,
                'smtp_user' => 'vivekr@pentationanalytics.com',
                'smtp_pass' => 'Vive@1234',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1'
            );

            $CI->load->library('email');
            $CI->email->initialize($config);

            $fromaddr = 'vivekr@pentationanalytics.com';
            $CI->email->from($fromaddr, $sendername);
            $CI->email->to($to);
            if (trim($cc) != '') {
                $CI->email->cc($cc);
            }
            if (trim($bcc) != '') {
                $CI->email->bcc($bcc);
            }
            $CI->email->subject($subject);

            $message = $CI->load->view('../views/mail/' . $view_name, $email_data, TRUE);

            $CI->email->message($message);

            if (trim($cc) != '') {
                $CI->email->cc($cc);
            }
            if (trim($bcc) != '') {
                $CI->email->bcc($bcc);
            }

            $CI->email->send();
            //echo $CI->email->print_debugger(); exit;
            return TRUE;
    }

}