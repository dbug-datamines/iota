<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class SettingLoader {
    /*
      |--------------------------------------------------------------------------
      | This sets the initial setting params
      |--------------------------------------------------------------------------
     */

    function initialize() {
        $ci = & get_instance();
        $ci->load->library('session');
		
		
		if ($ci->session->userdata('is_logged_in_agent')) {
            
			$response = $ci->session->userdata('agent_data');
			
			$ci->template->assign('user_details', $response);
        }
		
        $seg1 = $ci->uri->segment(1);
        $seg2 = $ci->uri->segment(2);
		
    }

}
